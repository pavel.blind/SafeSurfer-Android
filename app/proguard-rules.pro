# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in E:\Dev\AndroidSDK/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-dontwarn java.awt.*

# See http://stackoverflow.com/questions/5701126, happens in dnsjava
-keep class android.support.v7.widget.SearchView { *; }
-keep class com.safesurfer.util.GenericFileProvider
-keep class com.safesurfer.activities.PinActivity
-keep class android.support.v7.app.AppCompatViewInflater {
    *;
}
-renamesourcefileattribute SourceFile
-keepattributes SourceFile,LineNumberTable

-dontwarn org.slf4j.impl.*
-keep class org.slf4j.** {
    *;
}
-keep class org.pcap4j.** {
    *;
}
-assumenosideeffects class org.slf4j.Logger {
    public void debug(...);
    public void trace(...);
}