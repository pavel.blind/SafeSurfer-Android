package com.safesurfer.services;

/**
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.safesurfer.BuildConfig;
import com.safesurfer.network_api.SafeSurferNetworkApi;
import com.safesurfer.network_api.SafeSurferRepo;
import com.safesurfer.util.Constants;

import java.io.StringBufferInputStream;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.RequiresApi;

import org.json.JSONObject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.ResponseBody;

import static com.safesurfer.util.Constants.REGISTRATION_PREFERENCES_KEY_AUTH_DEVICE_TOKEN;
import static com.safesurfer.util.Constants.REGISTRATION_PREFERENCES_KEY_AUTH_TOKEN;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class PingService extends JobService {

    public static final String LOG_TAG = "PingService";

    @Override
    public boolean onStartJob(final JobParameters params) {
        Log.d(LOG_TAG, "Poll - onStartJob - ping api to update device ip on server");

//        Response.Listener<String> successResponse =   new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                jobFinished(params, true);
//                Log.d(LOG_TAG, "Poll - onResponse success");
//                Log.v(LOG_TAG, "Poll - onResponse success: " + response); //include the full response for verbose debugging.
//            }
//        };
//
//        Response.ErrorListener errResponse = new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                jobFinished(params, true);
//                Log.e(LOG_TAG, "Poll - Error calling api to update device IP address. OnErrorResponse:" + error.toString());
//            }
//        };

//       sendPingRequest(this, successResponse, errResponse);

//        SharedPreferences preferences = context.getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
//        final String uuid = preferences.getString(Constants.REGISTRATION_PREFERENCES_KEY_UDID, null);
//        final String macaddress = preferences.getString(Constants.REGISTRATION_PREFERENCES_KEY_MACADDRESS, "");

        SharedPreferences preferences = getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        String authToken = "Bearer " + preferences.getString(REGISTRATION_PREFERENCES_KEY_AUTH_DEVICE_TOKEN, null);

        SafeSurferNetworkApi.getInstance()
                .getSRUseCase()
                .updateDeviceIp(authToken, "0.0.0.0")
                .toObservable()
                .subscribe(new Observer<retrofit2.Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(retrofit2.Response<ResponseBody> responseBodyResponse) {
                        jobFinished(params, true);
                        try {
                            Log.d(LOG_TAG, "Poll - onResponse success");
                            Log.v(LOG_TAG, "Poll - onResponse success: " + responseBodyResponse.message()); //include the full response for verbose debugging.
                            if(responseBodyResponse.body() != null)Log.v(LOG_TAG, "Poll - onResponse success: " + responseBodyResponse.body().toString() ); //include the full response for verbose debugging.
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();

                        jobFinished(params, true);
                        Log.e(LOG_TAG, "Poll - Error calling api to update device IP address. OnErrorResponse:" );
                    }

                    @Override
                    public void onComplete() {

                    }
                });

       return true;
    }

    /**
     * Sends a ping request to the API to update the Device IP on the server
     *

     */
//    public void sendPingRequest(Context context, Response.Listener<String> successResponse, Response.ErrorListener errorResponse) {
//        SharedPreferences preferences = context.getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
//        final String uuid = preferences.getString(Constants.REGISTRATION_PREFERENCES_KEY_UDID, null);
//        final String macaddress = preferences.getString(Constants.REGISTRATION_PREFERENCES_KEY_MACADDRESS, "");
//
//        if (uuid == null)
//            return;
//
//        SafeSurferNetworkApi.getInstance()
//                .getSRUseCase()
//                .updateDeviceIp()
//                .toObservable()
//                .subscribe(new Observer<retrofit2.Response<ResponseBody>>() {
//                    @Override
//                    public void onSubscribe(Disposable d) {
//
//                    }
//
//                    @Override
//                    public void onNext(retrofit2.Response<ResponseBody> responseBodyResponse) {
//                        jobFinished(params, true);
//                        Log.d(LOG_TAG, "Poll - onResponse success");
//                        Log.v(LOG_TAG, "Poll - onResponse success: " + response); //include the full response for verbose debugging.
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        e.printStackTrace();
//                    }
//
//                    @Override
//                    public void onComplete() {
//
//                    }
//                });

//        RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//        StringRequest stringRequest = new StringRequest(
//                Request.Method.POST,
//                "https://www.safesurfer.co.nz/api/1.0/androidPoll.php",
//                successResponse,
//                errorResponse
//        ) {
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
////                headers.put("safesurfer_secret", BuildConfig.SAFESURFER_API_SECRET);
//                return headers;
//            }
//
//            @Override
//            public String getBodyContentType() {
//                return "application/x-www-form-urlencoded; charset=UTF-8";
//            }
//
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("deviceID", uuid);
//                params.put("deviceName", Build.MODEL);
//                params.put("mac", macaddress);
//                return params;
//            }
//
//        };
//
//        //do not cache the request to the server to ensure each time the Ping is done the request is sent
//        stringRequest.setShouldCache(false);
//        requestQueue.add(stringRequest);
//    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.d("PingService", "onStopJob");
        return true;
    }
}

