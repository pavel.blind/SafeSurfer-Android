package com.safesurfer.threading;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.VpnService;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.safesurfer.database.entities.IPPortPair;
import com.safesurfer.network_api.SafeSurferNetworkApi;
import com.safesurfer.services.DNSVpnService;

import android.system.OsConstants;
import android.util.Log;

import com.safesurfer.DNSChanger;
import com.safesurfer.LogFactory;
import com.safesurfer.screens.BackgroundVpnConfigureActivity;
import com.safesurfer.screens.InvalidDNSDialogActivity;
import com.safesurfer.util.PreferencesAccessor;
import com.safesurfer.util.dnsproxy.DNSProxy;
import com.safesurfer.util.dnsproxy.DummyProxy;
import com.frostnerd.general.StringUtil;
import com.frostnerd.networking.NetworkUtil;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Function;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.safesurfer.fragments.MainFragment.DEVICE_IS_PROTECTED;
import static com.safesurfer.fragments.MainFragment.DEVICE_IS_PROTECTED_VALUE;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 * <p>
 * This file is part of SafeSurfer-Android.
 * <p>
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class VPNRunnable implements Runnable {
    private static final String LOG_TAG = "[DNSVpnService-Runnable]";
    private static final Map<String, Integer> addresses = new ConcurrentHashMap<>();
    private final String ID = "[" + StringUtil.randomString(20) + "]";
    private int addressIndex = 0;
    private ParcelFileDescriptor tunnelInterface = null;
    private VpnService.Builder builder;
    private DNSVpnService service;
    private Set<String> vpnApps;
    private List<IPPortPair> upstreamServers;
    private boolean whitelistMode, running = true;
    private final List<Runnable> afterThreadStop = new ArrayList<>();
    private DNSProxy dnsProxy;

    static {
        addresses.put("172.31.255.253", 30);
        addresses.put("192.168.0.131", 24);
        addresses.put("192.168.234.55", 24);
        addresses.put("172.31.255.1", 28);
    }

    public VPNRunnable(@NonNull DNSVpnService service, @NonNull List<IPPortPair> upstreamServers, @NonNull Set<String> vpnApps, boolean whitelistMode) {
        if (service == null)
            throw new IllegalStateException("The DNSVPNService passed to VPNRunnable is null.");
        this.service = service;
        this.whitelistMode = whitelistMode;
        this.vpnApps = vpnApps;
        this.upstreamServers = new ArrayList<>(upstreamServers.size());
        for (IPPortPair pair : upstreamServers) { //Remap the loopback addresses to its replacements
            IPPortPair newPair = pair;
            if (pair.getAddress().equals("127.0.0.1")) {
                newPair = new IPPortPair(pair);
                newPair.setIp(DNSProxy.IPV4_LOOPBACK_REPLACEMENT);
            } else if (pair.getAddress().equals("::1")) {
                newPair = new IPPortPair(pair);
                newPair.setIp(DNSProxy.IPV6_LOOPBACK_REPLACEMENT);
            }
            this.upstreamServers.add(newPair);
        }
    }

    @Override
    public void run() {
        LogFactory.writeMessage(service, new String[]{LOG_TAG, "[VPNTHREAD]", ID}, "Starting Thread (run)");
        Thread.setDefaultUncaughtExceptionHandler(((DNSChanger) service.getApplicationContext()).getExceptionHandler());
        LogFactory.writeMessage(service, new String[]{LOG_TAG, "[VPNTHREAD]", ID}, "Trying " + addresses.size() + " different addresses before passing any thrown exception to the upper layer");
        try {
            for (String address : addresses.keySet()) {
                if (!running) break;
                addressIndex++;
                try {
                    LogFactory.writeMessage(service, new String[]{LOG_TAG, "[VPNTHREAD]", ID}, "Trying address '" + address + "'");
                    configure(address, PreferencesAccessor.isRunningInAdvancedMode(service));
                    tunnelInterface = builder.establish();
                    if (tunnelInterface == null) {
                        LogFactory.writeMessage(service, new String[]{LOG_TAG, "[VPNTHREAD]", ID}, "Tunnel interface is null, service is not prepared.");
                        LogFactory.writeMessage(service, new String[]{LOG_TAG, "[VPNTHREAD]", ID}, "Starting Background");
                        BackgroundVpnConfigureActivity.startBackgroundConfigure(service, true);
                        break;
                    }
                    checkServersReachability(new CheckServersReachability() {
                        @Override
                        public void checkComplete(int securityValue) {
                            Intent intent = new Intent(DEVICE_IS_PROTECTED);
                            Bundle bundle = new Bundle();
                            bundle.putBoolean(DEVICE_IS_PROTECTED_VALUE, securityValue > 0 );

                            intent.putExtra("extra", bundle );

                            service.sendBroadcast(intent);
                        }
                    });
                    LogFactory.writeMessage(service, new String[]{LOG_TAG, "[VPNTHREAD]", ID}, "Tunnel interface connected.");
                    LogFactory.writeMessage(service, new String[]{LOG_TAG, "[VPNTHREAD]", ID}, "Broadcasting current state");
                    service.broadcastCurrentState();
                    LogFactory.writeMessage(service, new String[]{LOG_TAG, "[VPNTHREAD]", ID}, "Broadcast sent");
                    service.updateNotification();
                    LogFactory.writeMessage(service, new String[]{LOG_TAG, "[VPNTHREAD]", ID}, "VPN Thread going into while loop");
                    if (PreferencesAccessor.isRunningInAdvancedMode(service) && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        LogFactory.writeMessage(service, new String[]{LOG_TAG, "[VPNTHREAD]", ID}, "We are in advanced mode, starting DNS proxy");
                        dnsProxy = DNSProxy.createProxy(service, tunnelInterface, new HashSet<>(upstreamServers)
                                , PreferencesAccessor.areRulesEnabled(service), PreferencesAccessor.isQueryLoggingEnabled(service),
                                PreferencesAccessor.logUpstreamDNSAnswers(service));
                        LogFactory.writeMessage(service, new String[]{LOG_TAG, "[VPNTHREAD]", ID}, "DNS proxy created");
                    } else dnsProxy = new DummyProxy();
                    dnsProxy.run();
                    LogFactory.writeMessage(service, new String[]{LOG_TAG, "[VPNTHREAD]", ID}, "VPN Thread reached end of while loop.");
                } catch (Exception e) {
                    if (!running) break;
                    LogFactory.writeStackTrace(service, new String[]{LOG_TAG, "[VPNTHREAD]", "[ADDRESS-RETRY]", ID}, e);
                    if (addressIndex >= addresses.size()) throw e;
                    else
                        LogFactory.writeMessage(service, new String[]{LOG_TAG, "[VPNTHREAD]", "[ADDRESS-RETRY]", ID},
                                "Not throwing exception. Tries: " + addressIndex + ", addresses: " + addresses.size());
                }
            }
        } catch (Exception e) {
            LogFactory.writeMessage(service, new String[]{LOG_TAG, "[VPNTHREAD]", ID}, "VPN Thread had an exception");
            LogFactory.writeStackTrace(service, new String[]{LOG_TAG, LogFactory.Tag.ERROR.toString()}, e);
            if (isDNSInvalid(e))
                service.startActivity(new Intent(service, InvalidDNSDialogActivity.class));
            else
                Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
        } finally {
            running = false;
            LogFactory.writeMessage(service, new String[]{LOG_TAG, "[VPNTHREAD]", ID}, "VPN Thread is in finally block");
            service.updateNotification();
            service.broadcastCurrentState();
            synchronized (afterThreadStop) {
                for (Runnable r : afterThreadStop) r.run();
                afterThreadStop.clear();
            }
            cleanup();
            LogFactory.writeMessage(service, new String[]{LOG_TAG, "[VPNTHREAD]", ID}, "Done with finally block");
        }
    }

    public void addAfterThreadStop(@NonNull Runnable runnable) {
        afterThreadStop.add(runnable);
    }

    private boolean isDNSInvalid(@NonNull Exception ex) {
        for (StackTraceElement ste : ex.getStackTrace())
            if (ste.toString().contains("Builder.addDnsServer") && ex instanceof IllegalArgumentException && ex.getMessage().contains("Bad address"))
                return true;
        return false;
    }

    private void cleanup() {
        running = false;
        if (tunnelInterface != null) {
            try {
                tunnelInterface.close();
            } catch (IOException | IllegalStateException e) {
                e.printStackTrace();
            }
        }
        builder = null;
        tunnelInterface = null;
        dnsProxy = null;
        builder = null;
    }

    private void configure(@NonNull String address, boolean advanced) {
        boolean ipv6Enabled = PreferencesAccessor.isIPv6Enabled(service), ipv4Enabled = PreferencesAccessor.isIPv4Enabled(service);
        if (!ipv4Enabled && !ipv6Enabled) ipv4Enabled = true;
        LogFactory.writeMessage(service, new String[]{LOG_TAG, "[VPNTHREAD]", ID}, "Creating Tunnel interface");
        builder = service.createBuilder();
        builder.setSession("Safe Surfer");
        if (advanced && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setBlocking(true);
        }
        if (ipv4Enabled) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder.allowFamily(OsConstants.AF_INET);
            }
            builder = builder.addAddress(address, addresses.get(address));
        }
        if (ipv6Enabled) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder.allowFamily(OsConstants.AF_INET6);
            }
            builder = builder.addAddress(NetworkUtil.randomLocalIPv6Address(), 48);
        }
        for (IPPortPair pair : upstreamServers) {
            if ((pair.isIpv6() && ipv6Enabled) || (!pair.isIpv6() && ipv4Enabled))
                addDNSServer(pair.getAddress(), advanced, pair.isIpv6());
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try {
                if (whitelistMode) {
                    for (String s : vpnApps) {
                        if (s.equals("com.android.vending")) continue;
                        builder = builder.addAllowedApplication(s);
                    }
                } else {
                    builder = builder.addDisallowedApplication("com.android.vending");
                    for (String s : vpnApps) {
                        if (s.equals("com.android.vending")) continue;
                        builder = builder.addDisallowedApplication(s);
                    }
                }
            } catch (PackageManager.NameNotFoundException ignored) {

            }
        }
        String release = Build.VERSION.RELEASE;
        if (Build.VERSION.SDK_INT != 19 || release.startsWith("4.4.3") || release.startsWith("4.4.4") || release.startsWith("4.4.5") || release.startsWith("4.4.6")) {
            builder.setMtu(1500);
        } else builder.setMtu(1280);
        LogFactory.writeMessage(service, new String[]{LOG_TAG, "[VPNTHREAD]", ID}, "Tunnel interface created, not yet connected");
        //builder.setConfigureIntent(PendingIntent.getActivity(service, 1, new Intent(service, PinActivity.class), PendingIntent.FLAG_CANCEL_CURRENT));
    }

    public static final Map<String, InetAddress> addressRemap = new HashMap<>();
    private final String addressRemapBase = "244.0.0.";
    private int addressRemapIndex = 1;

    private void addDNSServer(@NonNull String server, boolean addRoute, boolean ipv6) {
        if (server != null && !server.equals("")) {
            if (server.equals("127.0.0.1")) server = DNSProxy.IPV4_LOOPBACK_REPLACEMENT;
            else if (server.equals("::1")) server = DNSProxy.IPV6_LOOPBACK_REPLACEMENT;
            else if (PreferencesAccessor.sendDNSOverTLS(service)) {
                addressRemap.put(addressRemapBase + addressRemapIndex++, getRemappedAddress(server));
                server = addressRemapBase + (addressRemapIndex - 1);
            }
            builder.addDnsServer(server);
            if (addRoute) builder.addRoute(server, ipv6 ? 128 : 32);
        }
    }

    private InetAddress getRemappedAddress(String server) {
        try {
            return InetAddress.getByName(server);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean isThreadRunning() {
        return running;
    }

    public void stop(@Nullable Thread thread) {
        running = false;
        if (dnsProxy != null) dnsProxy.stop();
        if (thread != null) thread.interrupt();
    }

    public void destroy() {
        running = false;
        cleanup();
        vpnApps.clear();
        upstreamServers.clear();
        vpnApps = null;
        service = null;
    }


    int securityValue = 0;

    interface CheckServersReachability {
        public abstract void checkComplete(int securityValue);
    }

    private void checkServersReachability(CheckServersReachability listener) {
        securityValue = 0;

        ExecutorService service = Executors.newSingleThreadExecutor();
        service.submit(new Runnable() {
            @Override
            public void run() {
                SafeSurferNetworkApi.getInstance().getSRUseCase().checkConnectionSec_PlainDNS()
                        .toObservable()
                        .subscribe(success -> {
                            securityValue++;
                        }, fail -> {
                            fail.printStackTrace();
                        });
            }
        });

        service.submit(new Runnable() {
            @Override
            public void run() {
                SafeSurferNetworkApi.getInstance().getSRUseCase().checkConnectionSec_DNSCrypt()
                        .toObservable()
                        .subscribe(success -> {
                            securityValue++;
                        }, fail -> {
                            fail.printStackTrace();
                        });
            }
        });

        service.submit(new Runnable() {
            @Override
            public void run() {
                SafeSurferNetworkApi.getInstance().getSRUseCase().checkConnectionSec_DOH()
                        .toObservable()
                        .subscribe(success -> {
                            securityValue++;
                        }, fail -> {
                            fail.printStackTrace();
                        });
            }
        });

        service.submit(new Runnable() {
            @Override
            public void run() {
                SafeSurferNetworkApi.getInstance().getSRUseCase().checkConnectionSec_DOT()
                        .toObservable()
                        .subscribe(success -> {
                            securityValue++;
                        }, fail -> {
                            fail.printStackTrace();
                        });
            }
        });

        service.submit(new Runnable() {
            @Override
            public void run() {
                Log.d(getClass().getName(), String.format("secuty value is %s", String.valueOf(securityValue)));
                listener.checkComplete(securityValue);
            }
        });
    }
}
