package com.safesurfer.network_api;

import com.safesurfer.network_api.entities.DeviceAuthResponse;
import com.safesurfer.network_api.entities.GetBlockedSitesResponse;
import com.safesurfer.network_api.entities.GetCastEventsResponse;
import com.safesurfer.network_api.entities.GetCastsListResponse;
import com.safesurfer.network_api.entities.LogInResponse;
import com.safesurfer.network_api.entities.SetBlockedSitesRequestBody;

import io.reactivex.Observable;
import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.Header;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface NetworkClient {

    public abstract Single<Response<ResponseBody>> createNewUser(String email, String password);

    public abstract Single<Response<ResponseBody>> refreshJWTToken(String jwtToken);

    public abstract Single<Response<LogInResponse>> logIn(String email, String password, boolean enableSub);

    public abstract Single<Response<ResponseBody>> registerDevice(String jwtToken, String type, String name);

    public abstract Single<Response<DeviceAuthResponse>> deviceAuth(String authToken, String deviceID, String email, String password, boolean enableSub );

    public abstract Single<Response<GetBlockedSitesResponse>> getBlockedSites(String jwtToken);

    public abstract Single<Response<String>> screencastStart(String jwtToken);

    public abstract Single<Response<String>> screencastStop(String jwtToken);

    public abstract Single<Response<String>> addCastEvent(
            String jwtToken,
            String castId,
            long timestamp,
            String levels,
            String category,
            MultipartBody.Part image);

    public abstract Single<Response<GetCastEventsResponse>> getCastEventsList(String jwtToken, String castId, String page, String num_per_page);

    public abstract Single<Response<GetCastsListResponse>> getCastsList(String jwtToken,
                                                                        int page,
                                                                        int num_per_page,
                                                                        String device
    );

    Single<Response<String>> setBlockedSites(String jwtToken, SetBlockedSitesRequestBody body);

    Single<Response<ResponseBody>> requestCheck(String url);

    Single<Response<ResponseBody>> updateDeviceIp(String jwtToken, String deviceIp);

    Single<Response<ResponseBody>> getScreencasts(
            String jwtToken,
            String page,
            String num_per_page,
            String device
    );
}
