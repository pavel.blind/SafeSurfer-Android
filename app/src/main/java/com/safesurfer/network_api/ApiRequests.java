package com.safesurfer.network_api;

import com.safesurfer.network_api.entities.DeviceAuthResponse;
import com.safesurfer.network_api.entities.GetBlockedSitesResponse;
import com.safesurfer.network_api.entities.GetCastEventsResponse;
import com.safesurfer.network_api.entities.GetCastsListResponse;
import com.safesurfer.network_api.entities.LogInResponse;
import com.safesurfer.network_api.entities.SetBlockedSitesRequestBody;

import io.reactivex.Observable;
import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public class ApiRequests {


    public static interface CreateNewUser {
        @POST("/user")
        @FormUrlEncoded
        Single<Response<ResponseBody>> createNewUser(@Field("email") String email,
                                                     @Field("password") String password);
    }


    public static interface LogIn {
        @POST("/user/auth")
        @FormUrlEncoded
        Single<Response<LogInResponse>> logIn(@Field("email") String email,
                                              @Field("password") String password,
                                              @Field("enableSub") boolean enableSub
        );
    }

    public static interface RegisterDevice {
        @POST("/devices")
        @FormUrlEncoded
        Single<Response<ResponseBody>> registerDevice(@Header("Authorization") String jwtToken,
                                                      @Field("type") String type,
                                                      @Field("name") String name);
    }

    public static interface DeviceAuth {
        @POST("/user/device/auth")
        @FormUrlEncoded
        Single<Response<DeviceAuthResponse>> deviceAuth(
                @Header("Authorization") String jwtToken,
                @Field("deviceID") String deviceID,
                @Field("email") String email,
                @Field("password") String password,
                @Field("enableSub") boolean enableSub

        );
    }

    public static interface ScreencastStart {
        @POST("/screencasts/cast")
        Single<Response<String>> screencastStart(@Header("Authorization") String jwtToken);
    }

    public static interface ScreencastStop {
        @POST("/screencasts/cast/stop")
        Single<Response<String>> screencastStop(@Header("Authorization") String jwtToken);
    }

    /*
        @GET("users/{user_id}/playlists")
        Call<List<Playlist> getUserPlaylists(@Path(value = "user_id", encoded = true) String userId);
    */
    // /screencasts/cast/{castid}/events/event

    public static interface AddScreencastEvent {
        @POST("/screencasts/cast/{castid}/events/event")
        @Multipart
        Single<Response<String>> addCastEvent(@Header("Authorization") String jwtToken,
                                              @Path(value = "castid") String castId,
                                              @Part("timestamp") RequestBody timestamp,
                                              @Part("levels") RequestBody levels,
                                              @Part("category") RequestBody category,
                                              @Part MultipartBody.Part image
        );
    }

    // get      /screencasts/cast/{castid}/events

    public static interface GetScreencastEventsList {
        @GET("/screencasts/cast/{castid}/events")
        Single<Response<GetCastEventsResponse>> getCastEventsList(@Header("Authorization") String jwtToken,
                                                                  @Path(value = "castid") String castId,
                                                                  @Query("page") String page,
                                                                  @Query("num-per-page") String num_per_page
        );
    }

    public static interface GetCastsList {
        @GET("/screencasts")
        Single<Response<GetCastsListResponse>> getCastsList(@Header("Authorization") String jwtToken,
                                                            @Query("page") int page,
                                                            @Query("num-per-page") int num_per_page,
                                                            @Query("device") String device
        );
    }

    public static interface GetBlockedSites {
        @GET("/sites/blocked/")
        Single<Response<GetBlockedSitesResponse>> getBlockedSites(@Header("Authorization") String jwtToken);
    }

    public static interface SetBlockedSites {
        @POST("/sites/blocked/")
        @Headers({"Accept: application/json"})
        Single<Response<String>> setBlockedSites(
                @Header("Authorization") String jwtToken,
                @Body SetBlockedSitesRequestBody body
        );
    }

    public static interface CheckRequest {
        @GET("/")
        Single<Response<ResponseBody>> requestCheck();
    }

    public static interface UpdateDeviceIP {
        @PUT("/devices/device/ip")
      //  @FormUrlEncoded
        Single<Response<ResponseBody>> updateDeviceIp(
                @Header("Authorization") String jwtToken
                /*@Field("ip") String deviceIp*/
        );
    }

    public static interface GetScreencasts{
        @GET("/screencasts")
        Single<Response<ResponseBody>> getScreencasts(
                @Header("Authorization") String jwtToken,
                @Query("page") String page,
                @Query("num-per-page") String num_per_page,
                @Query("device") String device
        );
    }

    public static interface RefreshJWTToken {
        @GET("/user/subscriptions/refresh-jwt")
        Single<Response<ResponseBody>> refreshJWTToken(@Header("Authorization") String jwtToken);
    }

}
