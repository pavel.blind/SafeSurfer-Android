package com.safesurfer.network_api.entities;

import com.google.gson.annotations.SerializedName;

public class LogInResponse {

    @SerializedName("token")
    public String token;

}
