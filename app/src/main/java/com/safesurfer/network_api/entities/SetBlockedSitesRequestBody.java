package com.safesurfer.network_api.entities;

import com.google.gson.annotations.SerializedName;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class SetBlockedSitesRequestBody {

    @SerializedName("blacklist")
    public List<String> blacklist = new ArrayList<>();

    @SerializedName("deviceMac")
    public String deviceMac;

    @SerializedName("lifeguardID")
    public String lifeguardID;

    @SerializedName("overrideEnabled")
    public boolean overrideEnabled;

    @SerializedName("presetBlocked")
    public List<BigInteger> presetBlocked = new ArrayList<>();

    @SerializedName("presetNotBlocked")
    public List<BigInteger> presetNotBlocked = new ArrayList<>();

    @SerializedName("whitelist")
    public List<String> whitelist = new ArrayList<>();

    @SerializedName("youtube")
    public String youtube;
}
