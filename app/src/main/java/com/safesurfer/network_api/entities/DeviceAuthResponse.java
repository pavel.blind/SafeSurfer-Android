package com.safesurfer.network_api.entities;

import com.google.gson.annotations.SerializedName;

public class DeviceAuthResponse {

    @SerializedName("token")
    public String token;

}
