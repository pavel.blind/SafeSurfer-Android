package com.safesurfer.network_api.entities;

import com.google.gson.annotations.SerializedName;

import java.math.BigInteger;
import java.util.List;

public class GetCastsListResponse {

    @SerializedName("available")
    public BigInteger available;

    @SerializedName("casts")
    public List<Cast> casts;

    public static class Cast {

        @SerializedName("deviceID")
        public String deviceID;

        @SerializedName("endTimestamp")
        public BigInteger endTimestamp;

        @SerializedName("id")
        public BigInteger id;

        @SerializedName("lastEventTimestamp")
        public BigInteger lastEventTimestamp;

        @SerializedName("numEvents")
        public BigInteger numEvents;

        @SerializedName("startTimestamp")
        public BigInteger startTimestamp;

    }

}
