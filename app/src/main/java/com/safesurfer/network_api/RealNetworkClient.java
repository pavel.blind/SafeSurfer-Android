package com.safesurfer.network_api;

import com.google.gson.GsonBuilder;
import com.safesurfer.network_api.entities.DeviceAuthResponse;
import com.safesurfer.network_api.entities.GetBlockedSitesResponse;
import com.safesurfer.network_api.entities.GetCastEventsResponse;
import com.safesurfer.network_api.entities.GetCastsListResponse;
import com.safesurfer.network_api.entities.LogInResponse;
import com.safesurfer.network_api.entities.SetBlockedSitesRequestBody;

import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Header;
import retrofit2.http.Query;

public class RealNetworkClient implements NetworkClient {

    public static final String BASE_URL = "https://api.safesurfer.io";
//    public static final String BASE_URL = "https://staging.api.safesurfer.io/";

    private HttpLoggingInterceptor loggingInterceptor;
    private OkHttpClient httpClient;
    private Retrofit mRetrofitInstance;

    ApiRequests.CreateNewUser createNewUser;
    ApiRequests.LogIn logIn;
    ApiRequests.DeviceAuth deviceAuth;
    ApiRequests.RegisterDevice registerDevice;
    ApiRequests.GetBlockedSites getBlockedSites;
    ApiRequests.ScreencastStart startScreencast;
    ApiRequests.ScreencastStop stopScreencast;
    ApiRequests.AddScreencastEvent addScreencastEvent;
    ApiRequests.GetScreencastEventsList getScreencastEvents;
    ApiRequests.GetCastsList getCastsList;
    ApiRequests.SetBlockedSites setBlockedSites;
    ApiRequests.UpdateDeviceIP updateDeviceIp;
    ApiRequests.GetScreencasts getScreencasts;
    ApiRequests.RefreshJWTToken refreshJWTToken;

    public RealNetworkClient(){
        initRetroFit();
    }

    private void initRetroFit() {

        loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        httpClient = new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .readTimeout(10, TimeUnit.SECONDS)
                .connectTimeout(3, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();

        if (mRetrofitInstance == null) {
            mRetrofitInstance = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(httpClient)
                    .addCallAdapterFactory( RxJava2CallAdapterFactory.create() )
                    .addConverterFactory(createGsonConverter())
                    .build();
        }

        createNewUser = mRetrofitInstance.create(ApiRequests.CreateNewUser.class);
        logIn = mRetrofitInstance.create(ApiRequests.LogIn.class);
        deviceAuth = mRetrofitInstance.create(ApiRequests.DeviceAuth.class);
        registerDevice = mRetrofitInstance.create(ApiRequests.RegisterDevice.class);

        getBlockedSites = mRetrofitInstance.create(ApiRequests.GetBlockedSites.class);
        startScreencast = mRetrofitInstance.create(ApiRequests.ScreencastStart.class);
        stopScreencast = mRetrofitInstance.create(ApiRequests.ScreencastStop.class);
        addScreencastEvent = mRetrofitInstance.create(ApiRequests.AddScreencastEvent.class);
        getScreencastEvents = mRetrofitInstance.create(ApiRequests.GetScreencastEventsList.class);
        getCastsList = mRetrofitInstance.create(ApiRequests.GetCastsList.class);

        setBlockedSites = mRetrofitInstance.create(ApiRequests.SetBlockedSites.class);
        updateDeviceIp = mRetrofitInstance.create(ApiRequests.UpdateDeviceIP.class);
        getScreencasts = mRetrofitInstance.create(ApiRequests.GetScreencasts.class);
        refreshJWTToken = mRetrofitInstance.create(ApiRequests.RefreshJWTToken.class);

//        checkRequest = mRetrofitInstance.create(ApiRequests.CheckRequest.class);
    }

    private Converter.Factory  createGsonConverter(){
        GsonBuilder gsonBuilder = new GsonBuilder();
        // You can add multiple custom serialization adapters here
        return GsonConverterFactory.create(gsonBuilder.create());
    }

    @Override
    public Single<Response<ResponseBody>> createNewUser(String email, String password) {
        return createNewUser.createNewUser(email,password);
    }

    @Override
    public Single<Response<LogInResponse>> logIn(String email, String password, boolean enableSub) {
        return logIn.logIn(email,password,enableSub);
    }

    @Override
    public Single<Response<ResponseBody>> registerDevice(String jwtToken, String type, String name) {
        return registerDevice.registerDevice(jwtToken, type,name);
    }

    @Override
    public Single<Response<DeviceAuthResponse>> deviceAuth(String authToken, String deviceID, String email, String password, boolean enableSub ) {
        return deviceAuth.deviceAuth(authToken, deviceID, email, password, enableSub);
    }

    @Override
    public Single<Response<GetBlockedSitesResponse>> getBlockedSites(String jwtToken) {
        return getBlockedSites.getBlockedSites( jwtToken);
    }

    @Override
    public Single<Response<String>> screencastStart(String jwtToken) {
        return startScreencast.screencastStart(jwtToken);
    }

    @Override
    public Single<Response<String>> screencastStop(String jwtToken) {
        return stopScreencast.screencastStop(jwtToken);
    }

    @Override
    public Single<Response<String>> addCastEvent(String jwtToken, String castId, long timestamp, String levels, String category,  MultipartBody.Part image) {

        RequestBody requestBodyTimestamp = RequestBody.create( MediaType.parse("text/plain"), String.valueOf(timestamp) );
        RequestBody requestBodyLevels = RequestBody.create( MediaType.parse("text/plain"), String.valueOf(levels) );
        RequestBody requestBodyCategory = RequestBody.create( MediaType.parse("text/plain"), String.valueOf(category) );

        return addScreencastEvent.addCastEvent(jwtToken, castId, requestBodyTimestamp, requestBodyLevels, requestBodyCategory, image);
    }

    @Override
    public Single<Response<GetCastEventsResponse>> getCastEventsList(String jwtToken, String castId, String page, String num_per_page) {
        return getScreencastEvents.getCastEventsList(jwtToken, castId, page, num_per_page);
    }

    @Override
    public Single<Response<GetCastsListResponse>> getCastsList(String jwtToken, int page, int num_per_page, String device) {
        return getCastsList.getCastsList(jwtToken,page,num_per_page,device);
    }

    @Override
    public Single<Response<String>> setBlockedSites(String jwtToken, SetBlockedSitesRequestBody body) {
        return setBlockedSites.setBlockedSites(jwtToken,body);
    }

    @Override
    public Single <Response<ResponseBody>> requestCheck(String url) {
        Retrofit mRetrofitDynamicBaseUrlInstance = new Retrofit.Builder()
                .baseUrl(String.format("https://%s",url))
                .client(httpClient)
                .addCallAdapterFactory( RxJava2CallAdapterFactory.create() )
                /*.addConverterFactory(createGsonConverter())*/
                .build();
        ApiRequests.CheckRequest checkRequest = mRetrofitDynamicBaseUrlInstance.create(ApiRequests.CheckRequest.class);

        return checkRequest.requestCheck();
    }

    @Override
    public Single<Response<ResponseBody>> updateDeviceIp(String jwtToken, String deviceIp) {
        return updateDeviceIp.updateDeviceIp(jwtToken);
    }

    @Override
    public Single<Response<ResponseBody>> getScreencasts(String jwtToken, String page, String num_per_page, String device) {
        return getScreencasts.getScreencasts(jwtToken, page, num_per_page, device);
    }

    @Override
    public Single<Response<ResponseBody>> refreshJWTToken(String jwtToken) {
        return refreshJWTToken.refreshJWTToken(jwtToken);
    }
}
