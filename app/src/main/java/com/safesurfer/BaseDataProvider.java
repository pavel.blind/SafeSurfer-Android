package com.safesurfer;

import android.content.Context;

public class BaseDataProvider {

    public BaseDataProvider(Context context){
        setContext(context);
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    private Context context;


}
