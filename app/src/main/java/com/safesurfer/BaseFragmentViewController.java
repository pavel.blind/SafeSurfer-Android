package com.safesurfer;

import android.content.Context;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

public abstract class BaseFragmentViewController {

    public Fragment getFragment() {
        return mFragment;
    }

    public void setFragment(Fragment mFragment) {
        this.mFragment = mFragment;
    }

    private Fragment mFragment;

    public BaseFragmentViewController(Fragment fragment){
        setContext(fragment.getActivity());
        setFragment(fragment);
    }

    public Context getContext() {
        return context;
    }

    public AppCompatActivity getActivity() {
        return ((AppCompatActivity)context);
    }

    public void setContext(Context context) {
        this.context = context;
    }

    private Context context;

    public abstract void setUpView();

    public abstract View getRootView();

}
