package com.safesurfer.util;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.safesurfer.database.entities.IPPortPair;

import java.util.ArrayList;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class PreferencesAccessor {

    public static boolean isIPv6Enabled(@NonNull Context context) {
        return Preferences.getInstance(context).getBoolean( "setting_ipv6_enabled", false);
    }

    public static boolean isIPv4Enabled(@NonNull Context context) {
        return Preferences.getInstance(context).getBoolean(  "setting_ipv4_enabled", true);
    }

    public static void setIPv4Enabled(@NonNull Context context, boolean enabled ){
        Preferences.getInstance(context).put(  "setting_ipv4_enabled", enabled);
    }

    public static void setIPv6Enabled(@NonNull Context context, boolean enabled ){
        Preferences.getInstance(context).put(  "setting_ipv6_enabled", enabled);
    }

    public static void setPinEnabled(@NonNull Context context, boolean enabled ){
        Preferences.getInstance(context).put(  "setting_pin_enabled", enabled);
    }

    public static void setPinOnAppStart(@NonNull Context context, boolean enabled ){
        Preferences.getInstance(context).put(  "pin_app", enabled);
    }

    public static void setPinOnNotify(@NonNull Context context, boolean enabled ){
        Preferences.getInstance(context).put(  "pin_notification", enabled);
    }

    public static void setPin(@NonNull Context context, String pin ){
        Preferences.getInstance(context).put(  "pin_value", pin);
    }

    public static boolean isFirstRun(@NonNull Context context){
        return Preferences.getInstance(context).getBoolean(  "first_run", false);
    }

    public static boolean isEverythingDisabled(@NonNull Context context){
        return Preferences.getInstance(context).getBoolean(  "everything_disabled", false);
    }

    public static boolean checkConnectivityOnStart(@NonNull Context context){
        return Preferences.getInstance(context).getBoolean(  "check_connectivity", true);
    }

    public static boolean isDebugEnabled(@NonNull Context context){
        return Preferences.getInstance(context).getBoolean(  "debug", false);
    }

    public static boolean shouldHideNotificationIcon(@NonNull Context context){
        return Preferences.getInstance(context).getBoolean(  "hide_notification_icon", false);
    }

    public static boolean isNotificationEnabled(@NonNull Context context){
        return Preferences.getInstance(context).getBoolean(  "setting_show_notification", false);
    }

    public static boolean isAdvancedModeEnabled(@NonNull Context context){
        return Preferences.getInstance(context).getBoolean(  "advanced_settings", false);
    }

    public static boolean isLoopbackAllowed(@NonNull Context context){
        return isAdvancedModeEnabled(context) &&
                Preferences.getInstance(context).getBoolean(  "loopback_allowed", false);
    }

    public static boolean isRunningInAdvancedMode(@NonNull Context context){
        return areCustomPortsEnabled(context) ||
                areRulesEnabled(context) ||
                isQueryLoggingEnabled(context) ||
                sendDNSOverTCP(context) ||
                isLoopbackAllowed(context) ||
                sendDNSOverTLS(context) ||
                logUpstreamDNSAnswers(context);
    }

    public static boolean areCustomPortsEnabled(@NonNull Context context){
        return isAdvancedModeEnabled(context) &&
                Preferences.getInstance(context).getBoolean(  "custom_port", false);
    }

    public static boolean areRulesEnabled(@NonNull Context context){
        return isAdvancedModeEnabled(context) &&
                Preferences.getInstance(context).getBoolean(  "rules_activated", false);
    }

    public static boolean isQueryLoggingEnabled(@NonNull Context context){
        return isAdvancedModeEnabled(context) &&
                Preferences.getInstance(context).getBoolean(  "query_logging", false);
    }

    public static boolean logUpstreamDNSAnswers(@NonNull Context context){
        return isAdvancedModeEnabled(context) &&
                Preferences.getInstance(context).getBoolean(  "upstream_query_logging", false);
    }

    public static boolean isPinProtectionEnabled(@NonNull Context context){
        return Preferences.getInstance(context).getBoolean(  "setting_pin_enabled", false);
    }

    @NonNull
    public static String getPinCode(@NonNull Context context){
        return Preferences.getInstance(context).getString(  "pin_value", "1234");
    }

    public static boolean canUseFingerprintForPin(@NonNull Context context){
        return Preferences.getInstance(context).getBoolean(  "pin_fingerprint", false);
    }

    public static boolean sendDNSOverTCP(@NonNull Context context){
        return isAdvancedModeEnabled(context) &&
                Preferences.getInstance(context).getBoolean(  "dns_over_tcp", false);
    }

    public static boolean sendDNSOverTLS(@NonNull Context context){
        return isAdvancedModeEnabled(context) &&
                Preferences.getInstance(context).getBoolean(  "dns_over_tls", false);
    }

    public static int getTCPTimeout(@NonNull Context context){
        return Integer.parseInt(Preferences.getInstance(context).getString( "tcp_timeout", "500"));
    }

    @NonNull
    private static String getDNS1(@NonNull Context context) {
        return isIPv4Enabled(context) ? Preferences.getInstance(context).getString(  "dns1", "104.197.28.121") : "";
    }

    @NonNull
    private static String getDNS2(@NonNull Context context) {
        return isIPv4Enabled(context) ? Preferences.getInstance(context).getString(  "dns2", "104.155.237.225") : "";
    }

    @NonNull
    private static String getDNS1V6(@NonNull Context context) {
        return isIPv6Enabled(context) ? Preferences.getInstance(context).getString(  "dns1-v6", "2a0d:2a00:1::") : "";
    }

    @NonNull
    private static String getDNS2V6(@NonNull Context context) {
        return isIPv6Enabled(context) ? Preferences.getInstance(context).getString(  "dns2-v6", "2a0d:2a00:2::") : "";
    }

    @NonNull
    public static boolean isPinProtected(@NonNull Context context, PinProtectable pinProtectable){
        return pinProtectable.isEnabled(context);
    }

    @NonNull
    public static ArrayList<IPPortPair> getAllDNSPairs(@NonNull final Context context, final boolean enabledOnly){
        return new ArrayList<IPPortPair>(){
            private final boolean customPorts = areCustomPortsEnabled(context);
            {
                if(!enabledOnly || isIPv4Enabled(context)){
                    addIfNotEmpty(getDNS1(context), 1);
                    addIfNotEmpty(getDNS2(context), 3);
                }
                if(!enabledOnly || isIPv6Enabled(context)){
                    addIfNotEmpty(getDNS1V6(context), 2);
                    addIfNotEmpty(getDNS2V6(context), 4);
                }
            }
            private void addIfNotEmpty(String s, int id) {
                if (s != null && !s.equals("")) {
                    int port = customPorts ?
                            Preferences.getInstance(context).getInteger(  "port" + (id >= 3 ? "2" : "1") + (id % 2 == 0 ? "v6" : ""), 53)
                            : 53;
                    add(new IPPortPair(s, port, id % 2 == 0));
                }
            }
        };
    }

    public static boolean shouldShowVPNInfoDialog(@NonNull Context context){
        return Preferences.getInstance(context).getBoolean(  "show_vpn_info", true);
    }

    public static void setShowVPNInfoDialog(@NonNull Context context, boolean doShow){
        Preferences.getInstance(context).put(  "show_vpn_info", doShow);
    }

    public enum Type{
        DNS1("dns1", "port1", "104.197.28.121"),
        DNS2("dns2", "port2", "104.155.237.225"),
        DNS1_V6("dns1-v6", "port1v6", "2001:4860:4860::8888"),
        DNS2_V6("dns2-v6", "port2v6", "2001:4860:4860::8844");

        private final String dnsKey, portKey, defaultAddress;
        Type(@NonNull String dnsKey, @NonNull String portKey, @NonNull String defaultAddress){
            this.dnsKey = dnsKey;
            this.portKey = portKey;
            this.defaultAddress = defaultAddress;
        }

        @Nullable
        public static Type fromKey(@NonNull String key){
            for(Type val: values()){
                if(val.dnsKey.equalsIgnoreCase(key))return val;
            }
            return null;
        }

        private boolean isIPv6(){
            return dnsKey.contains("v6");
        }

        private boolean isAddressTypeEnabled(@NonNull Context context){
            return (isIPv6() && isIPv6Enabled(context)) || (!isIPv6() && isIPv4Enabled(context));
        }

        private int getPort(@NonNull Context context){
            return areCustomPortsEnabled(context) ? Preferences.getInstance(context).getInteger(  portKey, 53) : 53;
        }

        @NonNull
        private String getServerAddress(@NonNull Context context){
            return Preferences.getInstance(context).getString(  dnsKey, defaultAddress);
        }

        @NonNull
        public IPPortPair getPair(@NonNull Context context){
            return new IPPortPair(getServerAddress(context), getPort(context), isIPv6());
        }

        public void saveDNSPair(@NonNull Context context, @NonNull IPPortPair pair){
            if(!canBeEmpty() && pair.isEmpty())return;
            Preferences.getInstance(context).put(  portKey, pair.getPort() == -1 ? 53 : pair.getPort());
            Preferences.getInstance(context).put(  dnsKey, pair.getAddress());
        }

        public boolean canBeEmpty(){
            return this == DNS2 || this == DNS2_V6;
        }
    }

    public enum PinProtectable{
        APP("pin_app"), APP_SHORTCUT("pin_app_shortcut"), TILE("pin_tile"), NOTIFICATION("pin_notification");

        @NonNull private final String settingsKey;
        PinProtectable(@NonNull String settingsKey){
            this.settingsKey = settingsKey;
        }

        private boolean isEnabled(Context context){
            return Preferences.getInstance(context).getBoolean(  "setting_pin_enabled", false) &&
                    Preferences.getInstance(context).getBoolean(  settingsKey, false);
        }

    }
}
