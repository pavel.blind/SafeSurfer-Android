package com.safesurfer.util;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */

public class Constants {

    public static String PREFS_NAME = "lock";
    //Prefrences Tag
    public static String TAG_LOCK="LOCK";
    public static boolean LOCK_STATUS=false;
    public static String PIN="pin";
    public static String TEMP_PIN="temppin";
    public static String isNewUser="new user";

    public static String isInConfirmProcess="isConfirmProcess";

    public static String REGISTRATION_PREFERENCES = "com.safesurfer.registration";
    public static String REGISTRATION_PREFERENCES_KEY_UDID = "com.safesurfer.registration.udid"; // Android Secure ID
    public static String REGISTRATION_PREFERENCES_KEY_MACADDRESS = "com.safesurfer.registration.macaddress";
    public static String REGISTRATION_PREFERENCES_KEY_EMAIL = "com.safesurfer.registration.email";
    public static String REGISTRATION_PREFERENCES_KEY_PASSWORD = "com.safesurfer.registration.password";
    public static String REGISTRATION_PREFERENCES_KEY_ISFIRSTTIME = "com.safesurfer.registration.isfirsttime";

    public static String REGISTRATION_PREFERENCES_KEY_DEVICEID = "com.safesurfer.registration.device_id";
    public static String REGISTRATION_PREFERENCES_KEY_AUTH_TOKEN = "com.safesurfer.registration.auth_token";
    public static String REGISTRATION_PREFERENCES_KEY_AUTH_DEVICE_TOKEN = "com.safesurfer.registration.auth_device_token";

    public static String REGISTRATION_PREFERENCES_KEY_SCREENCAST_ID = "com.safesurfer.registration.screencast_id";

    public static String SHARED_PREFERENCES_NAME = "pref";
    public static String SHARED_PREFERENCES_KEY_DISALLOW = "com.safesurfer.key.disallow";
}
