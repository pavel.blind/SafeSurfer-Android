package com.safesurfer.util.dnsproxy;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class DummyProxy extends DNSProxy {
    private boolean shouldRun = true;

    public DummyProxy(){
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CREATING DUMMY");
    }

    @Override
    public void run() throws InterruptedException {
        while(shouldRun){
            Thread.sleep(250);
        }
    }

    @Override
    public void stop() {
        shouldRun = false;
    }
}
