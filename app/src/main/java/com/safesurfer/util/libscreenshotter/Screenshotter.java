package com.safesurfer.util.libscreenshotter;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.Image;
import android.media.ImageReader;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.util.Log;

import com.safesurfer.util.Tools;
import com.safesurfer.util.libscreenshotter.scheduler.ScreencastServiceScheduler;

import java.nio.Buffer;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by omerjerk on 17/2/16.
 */
public class Screenshotter implements ImageReader.OnImageAvailableListener {

    private static final String TAG = "LibScreenshotter";

    private VirtualDisplay virtualDisplay;

    private int width;
    private int height;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    private Context context;

    private int resultCode;
    private Intent data;

    private static Screenshotter mInstance;

    private ImageReader mImageReader;
    private MediaProjection mMediaProjection;
    private volatile int imageAvailable = 0;

    /**
     * Get the single instance of the Screenshotter class.
     *
     * @return the instance
     */
    public static Screenshotter getInstance() {
        if (mInstance == null) {
            mInstance = new Screenshotter();
        }
        return mInstance;
    }

    private Screenshotter() {
    }

    /**
     * Takes the screenshot of whatever currently is on the default display.
     *
     * @param resultCode The result code returned by the request for accessing MediaProjection permission
     * @param data       The intent returned by the same request
     */
    MediaProjectionManager mediaProjectionManager;

    public Screenshotter takeScreenshot(Context context, int resultCode, Intent data) {
        this.context = context;
        this.resultCode = resultCode;
        this.data = data;

        ScreenshotterDatabase.getInstance().setContext(context);

        ScreenshotterDatabase.getInstance().initTensorFlowAndLoadModel();

        imageAvailable = 0;
        mImageReader = ImageReader.newInstance(width, height, PixelFormat.RGBA_8888, 1);

        mediaProjectionManager = (MediaProjectionManager) context.getSystemService(Context.MEDIA_PROJECTION_SERVICE);
        if (mMediaProjection == null) {
            mMediaProjection = mediaProjectionManager.getMediaProjection(this.resultCode, this.data);
            if (mMediaProjection == null) {
                Log.e(TAG, "MediaProjection null. Cannot take the screenshot.");
                return this;
            }
        }
        try {
            virtualDisplay = mMediaProjection.createVirtualDisplay(
                    "Screenshotter",
                    width,
                    height,
                    50,
                    DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR,
                    mImageReader.getSurface(), null, null);

            mImageReader.setOnImageAvailableListener(Screenshotter.this, null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return this;
    }

    /**
     * Set the size of the screenshot to be taken
     *
     * @param width  width of the requested bitmap
     * @param height height of the request bitmap
     * @return the singleton instance
     */
    public Screenshotter setSize(int width, int height) {
        this.width = width;
        this.height = height;
        return this;
    }

    int imageCounter = 0;

    @Override
    public void onImageAvailable(final ImageReader reader) {

        Log.d(getClass().getName(), "public void onImageAvailable(ImageReader reader) {}");
        if (imageCounter != 0) return;

        ScreenshotterDatabase.getInstance().getmSingleThreadExecutor().submit(new Runnable() {
            @Override
            public void run() {
                Image image;
                if (reader.getMaxImages() > 1) {
                    image = reader.acquireLatestImage();
                } else {
                    image = reader.acquireNextImage();
                }

//                if (image == null) return;

                final Image.Plane[] planes = image.getPlanes();
                final Buffer buffer = planes[0].getBuffer().rewind();
                int pixelStride = planes[0].getPixelStride();
                int rowStride = planes[0].getRowStride();

                int rowPadding = rowStride - pixelStride * width;

                // create bitmap
                Bitmap bitmap = Bitmap.createBitmap(width + rowPadding / pixelStride, height, Bitmap.Config.ARGB_8888);
                bitmap.copyPixelsFromBuffer(buffer);

                image.close();

                ScreenshotterDatabase.getInstance().onNewScreenshot(bitmap, new ScreenshotterDatabase.ScreenshotterDatabaseListener() {
                    @Override
                    public void onBitmapAnalyzed() {
//                        if(Tools.getInstance().getScreencastMode() == Tools.SCREENCAST_MODE_ALWAYS ){
//                            Tools.getInstance().scheduleJob(getContext() );
//                            tearDown();
//                        }
                    }

                    @Override
                    public void onBitmapIsSuggestive() {
                        getListener().onBitmapIsSuggestive();
                    }

                    @Override
                    public void onBitmapIsExplicit() {
                        getListener().onBitmapIsExplicit();
                    }

                    @Override
                    public void bitmapAnalyzed(float neutral, float suggestive, float explicit) {

                        getDebugModeListener().bitmapAnalyzed(neutral, suggestive, explicit);
                    }
                });
            }
        });

    }

    public ScreenshotterDatabase.ScreenshotterDatabaseListener getListener() {
        if(listener == null)return new ScreenshotterDatabase.ScreenshotterDatabaseListener() {
            @Override
            public void onBitmapIsSuggestive() {

            }

            @Override
            public void onBitmapIsExplicit() {

            }

            @Override
            public void onBitmapAnalyzed() {

            }

            @Override
            public void bitmapAnalyzed(float neutral, float suggestive, float explicit) {

            }
        };
        return listener;
    }

    public void setListener(ScreenshotterDatabase.ScreenshotterDatabaseListener listener) {
        this.listener = listener;
    }

    private ScreenshotterDatabase.ScreenshotterDatabaseListener listener;

    public void tearDown() {
        if(virtualDisplay != null)virtualDisplay.release();
        if(mMediaProjection != null)mMediaProjection.stop();

        mMediaProjection = null;
        mImageReader = null;
    }

    public DebugModeListener getDebugModeListener() {
        if(debugModeListener == null)return new DebugModeListener() {
            @Override
            public void bitmapAnalyzed(float neutral, float suggestive, float explicit) {

            }
        };
        return debugModeListener;
    }

    public void setDebugModeListener(DebugModeListener debugModeListener) {
        this.debugModeListener = debugModeListener;
    }

    private DebugModeListener debugModeListener;

    public static interface DebugModeListener{
        public abstract void bitmapAnalyzed(float neutral, float suggestive, float explicit);
    }

}

