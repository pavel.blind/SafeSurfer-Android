package com.safesurfer.util.libscreenshotter;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.safesurfer.R;
import com.safesurfer.network_api.SafeSurferNetworkApi;
import com.safesurfer.screens.events_history.events_history_item.EventDetailsActivity;
import com.safesurfer.tensorflow.EventModel;
import com.safesurfer.tensorflow.EventModelScores;
import com.safesurfer.tensorflow.EventsRepo;
import com.safesurfer.util.Tools;

import org.tensorflow.lite.DataType;
import org.tensorflow.lite.Interpreter;
import org.tensorflow.lite.support.common.FileUtil;
import org.tensorflow.lite.support.image.ImageProcessor;
import org.tensorflow.lite.support.image.TensorImage;
import org.tensorflow.lite.support.image.ops.ResizeOp;
import org.tensorflow.lite.support.label.TensorLabel;
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.MappedByteBuffer;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.realm.Realm;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.safesurfer.tensorflow.EventModel.EventModelTypesEXPLICIT;
import static com.safesurfer.tensorflow.EventModel.EventModelTypesNEUTRAL;
import static com.safesurfer.tensorflow.EventModel.EventModelTypesSUGGESTIVE;
import static com.safesurfer.util.Tools.NOTIFICATION_ID;

public class ScreenshotterDatabase {

    public static final int WATCHING_MODE_PERIOD_MILLIS = 1000;
    boolean canMakeScreenshot = true;

    private final int RECORDING_LEVEL_NONE = 1;
    private final int RECORDING_LEVEL_SCORES = 2;
    private final int RECORDING_LEVEL_SCORES_SCREENS = 3;
    private static final String MODEL_PATH = "neutral-porn-suggestive-classifier.tflite";
    private static final boolean QUANT = false;
    private static final int INPUT_SIZE = 224;

    final String ASSOCIATED_AXIS_LABELS = "labels.txt";
    List<String> associatedAxisLabels = null;

    Handler mUIHandler = new Handler();
    private Executor executor = Executors.newSingleThreadExecutor();

    ImageProcessor imageProcessor;
    MappedByteBuffer tfliteModel;
    Interpreter tflite;

    Context context;

    ExecutorService mSingleThreadExecutor = Executors.newSingleThreadExecutor();

    private static ScreenshotterDatabase instance;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }


    public ExecutorService getmSingleThreadExecutor() {
        return mSingleThreadExecutor;
    }

    private ScreenshotterDatabase() {
    }

    public static ScreenshotterDatabase getInstance() {
        if (instance == null) instance = new ScreenshotterDatabase();
        return instance;
    }

    public static interface ScreenshotterDatabaseListener {
        public abstract void onBitmapIsSuggestive();

        public abstract void onBitmapIsExplicit();

        public abstract void onBitmapAnalyzed();

        public abstract void bitmapAnalyzed(float neutral, float suggestive, float explicit);
    }


    public ScreenshotterDatabaseListener getmListener() {
        return mListener;
    }

    public void setmListener(ScreenshotterDatabaseListener mListener) {
        this.mListener = mListener;
    }

    private ScreenshotterDatabaseListener mListener;

    public void onNewScreenshot(Bitmap bitmap, ScreenshotterDatabaseListener listener) {
        setmListener(listener);
        try {

            if (!canMakeScreenshot) return;

            int cropRectangleWidth = 0;
            if (bitmap.getWidth() >= bitmap.getHeight()) cropRectangleWidth = bitmap.getHeight();
            if (bitmap.getWidth() < bitmap.getHeight()) cropRectangleWidth = bitmap.getWidth();

            Bitmap croppedBmp = Bitmap.createBitmap(bitmap,
                    bitmap.getWidth() / 2 - cropRectangleWidth / 2,
                    bitmap.getHeight() / 2 - cropRectangleWidth / 2,
                    cropRectangleWidth,
                    cropRectangleWidth);

//            bitmapToFile(croppedBmp);

            Bitmap scaledBitmap = Bitmap.createScaledBitmap(croppedBmp, INPUT_SIZE, INPUT_SIZE, true);

//            bitmapToFile(scaledBitmap);
            /*====================================================*/

            TensorImage tImage = new TensorImage(DataType.FLOAT32);
            tImage.load(scaledBitmap);
            tImage = imageProcessor.process(tImage);

            try {
                associatedAxisLabels = FileUtil.loadLabels(getContext(), ASSOCIATED_AXIS_LABELS);
            } catch (IOException e) {
                Log.e("tfliteSupport", "Error reading label file", e);
            }

            TensorBuffer probabilityBuffer = TensorBuffer.createFixedSize(new int[]{1, 3}, DataType.FLOAT32);
            if (null != tflite) {
                tflite.run(tImage.getBuffer(), probabilityBuffer.getBuffer());
            }

            // Post-processor which dequantize the result
//            TensorProcessor probabilityProcessor = new TensorProcessor.Builder().add(new NormalizeOp(0, 255)).build();

            if (null != associatedAxisLabels) {
                // Map of labels and their corresponding probability
                TensorLabel labels = new TensorLabel(associatedAxisLabels, probabilityBuffer);

                // Create a map to access the result based on label
                Map<String, Float> floatMap = labels.getMapWithFloatValue();

                Log.d(getClass().getName(), "======================================================");
                for (String key : floatMap.keySet()) {
                    Log.d(getClass().getName(), String.format("floatMap %s %.5f", key, floatMap.get(key)));
                }
                Log.d(getClass().getName(), "======================================================");

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                analyzePerformModel(byteArray, floatMap);
            }

            canMakeScreenshot = false;

            mUIHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    canMakeScreenshot = true;
                }
            }, WATCHING_MODE_PERIOD_MILLIS);


            getmListener().onBitmapAnalyzed();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void analyzePerformModel(final byte[] byteArray, final Map<String, Float> floatMap) {

        Date currentTime = Calendar.getInstance().getTime();
        final long timeStamp = currentTime.getTime();

        if (getRecordingLevel() == RECORDING_LEVEL_NONE) return;

        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                EventModelScores scores = new EventsRepo(context).getNewEventModelScoreIdentifier();

                Log.d(getClass().getName(), floatMap.keySet().toString());

                for (String key : floatMap.keySet()) {

                    if (key.equals("neutral")) {
                        Log.d(getClass().getName(), "key " + key);
                        float value = floatMap.get(key) * 100f;
                        Log.d(getClass().getName(), String.format("value %.5f ", value));

                        scores.setNeutral(value);
                    }
                    if (key.equals("porn")) {
                        Log.d(getClass().getName(), "key " + key);
                        float value = floatMap.get(key) * 100f;
                        Log.d(getClass().getName(), String.format("value %.5f ", value));

                        scores.setExplicit(value);
                    }
                    if (key.equals("suggestive")) {
                        Log.d(getClass().getName(), "key " + key);
                        float value = floatMap.get(key) * 100f;
                        Log.d(getClass().getName(), String.format("value %.5f ", value));

                        scores.setSuggestive(value);
                    }
                }

                getmListener().bitmapAnalyzed(scores.getNeutral(),
                        scores.getSuggestive(),
                        scores.getExplicit());

                int currentModelType = EventModelTypesNEUTRAL;

                Log.d(getClass().getName(), "(int) scores.getNeutral() " + String.valueOf(scores.getNeutral()));
                Log.d(getClass().getName(), "getNeutralThreshold() " + String.valueOf(Tools.getInstance().getNeutralThreshold()));

                if ((int) scores.getNeutral() < Tools.getInstance().getNeutralThreshold()) {

                    Log.d(getClass().getName(), "(int) scores.getSuggestive() " + String.valueOf(scores.getSuggestive()));
                    Log.d(getClass().getName(), "getSuggestiveThreshold() " + String.valueOf(Tools.getInstance().getSuggestiveThreshold()));

                    if ((int) scores.getSuggestive() > Tools.getInstance().getSuggestiveThreshold()) {
                        currentModelType = EventModelTypesSUGGESTIVE;
                    }

                    Log.d(getClass().getName(), "scores.getExplicit() " + String.valueOf(scores.getExplicit()));
                    Log.d(getClass().getName(), "getExplicitThreshold() " + String.valueOf(Tools.getInstance().getExplicitThreshold()));

                    if ((int) scores.getExplicit() > Tools.getInstance().getExplicitThreshold()) {
                        currentModelType = EventModelTypesEXPLICIT;
                    }
                }

                /*====================================================================================*/
                /*====================================================================================*/
                /*====================================================================================*/
                if (currentModelType == EventModelTypesNEUTRAL) return;

                /*=========================================================*/
                /*=========================================================*/
                EventModel model = new EventsRepo(context).getNewEventModel();
                model.setTimestamp(timeStamp);
                model.setScores(scores);
//                Explicit: score - 78%, threshold - 40%"
                /*=====================================================================================*/
                if (currentModelType == EventModelTypesSUGGESTIVE) {
                    model.setEventModeltype(EventModelTypesSUGGESTIVE);

                    String part1 = "Suggestive: score - ";
                    String part2 = "%, threshold - ";
                    String part3 = "%";

                    String result = String.format("%s%s%s%s%s", part1, String.valueOf((int) scores.getSuggestive()), part2, String.valueOf(Tools.getInstance().getSuggestiveThreshold()), part3);

                    createNotification(result, model.identifier);

                    getmListener().onBitmapIsSuggestive();
                }
                if (currentModelType == EventModelTypesEXPLICIT) {
                    model.setEventModeltype(EventModelTypesEXPLICIT);
                    String part1 = "Explicit: score - ";
                    String part2 = "%, threshold - ";
                    String part3 = "%";

                    String result = String.format("%s%s%s%s%s", part1, String.valueOf((int) scores.getExplicit()), part2, String.valueOf(Tools.getInstance().getExplicitThreshold()), part3);

                    createNotification(result, model.identifier);

                    getmListener().onBitmapIsExplicit();
                }
                /*====================================================================================*/
                if (getRecordingLevel() == RECORDING_LEVEL_SCORES_SCREENS) {
                    model.setImageArray(byteArray);
                }

                String deviceAuthToken = Tools.getInstance().getDeviceAuthToken();
                String currentCastId = Tools.getInstance().getCurrentScreencastId();


                if (deviceAuthToken != null & currentCastId != null) {

                    long timestamp = System.currentTimeMillis();
                    RequestBody requestFile = RequestBody.create(MediaType.parse("image/png"), byteArray);

                    MultipartBody.Part imagePart = MultipartBody.Part.createFormData("image", "image", requestFile);
                    SafeSurferNetworkApi.getInstance().getSRUseCase().addCastEvent(
                            deviceAuthToken,
                            currentCastId,
                            timestamp /1000, // попросили присылать секунды
                            scores,
                            model.getEventModelTypeName(),
                            imagePart
                    ).toObservable().subscribe(success -> {
                        Log.d(getClass().getName(), "addCastEvent success ");

                    }, fail -> {
                        fail.printStackTrace();
                    });
                }

            }
        });
    }

    private void createNotification(String contentText, int eventid) {

        if (!Tools.getInstance().isScreencastDetectionNotificationsActive()) return;


//
//        String CHANNEL_ID = Tools.getInstance().getNotificationChannelId();
//
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            NotificationChannel channel = new NotificationChannel( CHANNEL_ID, "Safe surfer", NotificationManager.IMPORTANCE_LOW);
//            channel.setDescription("Safe surfer");
//            channel.enableLights(false);
//            channel.setVibrationPattern(new long[]{ 0 });
//            channel.setSound(null, null);
//            channel.enableVibration(true);
//
//            notificationManager.createNotificationChannel(channel);
//        }
        NotificationManager notificationManager = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        Tools.getInstance().createNotificationChannel(notificationManager);

        Intent notifyIntent = EventDetailsActivity.getIntent(getContext(), eventid);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        notifyIntent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK );

        PendingIntent notifyPendingIntent = PendingIntent.getActivity(getContext(), 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, Tools.getInstance().getNotificationChannelId())
                .setSmallIcon(R.drawable.ic_stat_name)
                .setColor(Color.parseColor("#00A7D6"))
                .setContentTitle("Safe surfer")
                .setContentIntent(notifyPendingIntent)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setVibrate(new long[]{0L})
                .setContentText(contentText);

        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }

    public void initTensorFlowAndLoadModel() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                imageProcessor = new ImageProcessor.Builder()
                        .add(new ResizeOp(INPUT_SIZE, INPUT_SIZE, ResizeOp.ResizeMethod.BILINEAR))
                        .build();
                try {
                    tfliteModel = FileUtil.loadMappedFile(getContext(), MODEL_PATH);
                    tflite = new Interpreter(tfliteModel);
                } catch (IOException e) {
                    Log.e("tfliteSupport", "Error reading model", e);
                }
            }
        });
    }

    private void bitmapToFile(Bitmap bitmap) {
        String fileName = String.valueOf(System.currentTimeMillis());
        try {
            String path = Environment.getExternalStorageDirectory().toString() + "/develop/";
            new File(path).mkdir();
            OutputStream fOut = null;
            Integer counter = 0;
            File file = new File(path, fileName + ".png"); // the File to save , append increasing numeric counter to prevent files from getting overwritten.
            fOut = new FileOutputStream(file);

            bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut); // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
            fOut.flush(); // Not really required
            fOut.close(); // do not forget to close the stream

            MediaStore.Images.Media.insertImage(getContext().getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int getRecordingLevel() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        return Integer.parseInt(sharedPref.getString("recording_level", "1"));
    }

//    private void rescheduleJob(Context context) {
//        try {
//            Tools.getInstance().scheduleJob();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


}
