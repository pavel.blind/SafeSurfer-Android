package com.safesurfer.util.libscreenshotter.scheduler;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.util.Log;

public class ScreencastServiceScheduler extends JobService {

    private static final String TAG = ScreencastServiceScheduler.class.getSimpleName();

    public ScreencastServiceScheduler() {

    }

    @Override
    public boolean onStartJob(JobParameters params) {

        Log.d(TAG, "onStartJob: starting job with id: " + params.getJobId());

        ScreencastSchedulerService.startAction(getApplicationContext());

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.d(TAG, "onStopJob: stopping job with id: " + params.getJobId());
        return true;
    }
}
