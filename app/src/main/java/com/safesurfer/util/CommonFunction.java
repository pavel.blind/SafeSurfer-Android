package com.safesurfer.util;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Vibrator;
import android.widget.Toast;


public class CommonFunction {

    public static void vibrator(Context context){
        // Get instance of Vibrator from current Context
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

        v.vibrate(100);
    }
    public static void saveRegisterDetails(Activity activity, String FNAME, String LNAME, String EMAIL, String EMAIL2){
        SharedPreferences settings;
        SharedPreferences.Editor preferences;
        settings = activity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE); //1
        preferences = settings.edit();
        preferences.putString("FNAME",FNAME);
        preferences.putString("LNAME",LNAME);
        preferences.putString("EMAIL",EMAIL);
        preferences.putString("EMAIL2",EMAIL2);

        preferences.commit();
    }
    public static void showmsg(Activity activity,String msg){
        Toast.makeText(activity,msg,Toast.LENGTH_SHORT).show();
    }

    public static void saveAuthenticationDetails(Activity activity, String ques_1, String ques_2, String ques_3,
                                                 String ans_1, String ans_2, String ans_3) {

        SharedPreferences settings;
        SharedPreferences.Editor preferences;
        settings = activity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE); //1
        preferences = settings.edit();
        preferences.putString("ques_1",ques_1);
        preferences.putString("ques_2",ques_2);
        preferences.putString("ques_3",ques_3);
        preferences.putString("ans_1",ans_1);
        preferences.putString("ans_2",ans_2);
        preferences.putString("ans_3",ans_3);
        preferences.commit();

    }
    public static boolean getAuthenticationDetails(Activity activity) {
        String ques_1,  ques_2,  ques_3,
                ans_1,  ans_2,  ans_3;
        SharedPreferences settings;
        SharedPreferences.Editor preferences;
        settings = activity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE); //1
        ques_1 = settings.getString("ques_1","");
        ques_2 = settings.getString("ques_2","");
        ques_3 = settings.getString("ques_3","");
        ans_1 = settings.getString("ans_1","");
        ans_2 = settings.getString("ans_2","");
        ans_3= settings.getString("ans_3","");
        if(ques_1.length()>0 && ques_2.length()>0 && ques_3.length()>0
                && ans_1.length()>0 && ans_2.length()>0 && ans_3.length()>0 ){
            return  true;
        }else{
            return  false;
        }
    }

    public static void saveSecurtiyCodeDetails(Activity activity, String s_code) {
        SharedPreferences settings;
        SharedPreferences.Editor preferences;
        settings = activity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE); //1
        preferences = settings.edit();
        preferences.putString("s_code",s_code);
        preferences.commit();
    }

    public static void saveLockDetails(Activity activity, boolean _islock) {
        SharedPreferences settings;
        SharedPreferences.Editor preferences;
        settings = activity.getSharedPreferences(Constants.PREFS_NAME, Context.MODE_PRIVATE); //1
        preferences = settings.edit();
        preferences.putBoolean(Constants.TAG_LOCK,_islock);
        preferences.commit();
    }

    //TODO-Internet connection Check
    public static boolean isConnectingToInternet(Context _context){
        ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }

        }
        return false;
    }


    //TODO-Alert Dialog
    public static void alert_Internet(Context context){
        new AlertDialog.Builder(context)
                .setTitle("Alert")
                .setMessage("Internet connection not available")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();
                    }
                })
                .setCancelable(false)
                .show();
    }




    public static boolean isTablet(Context context) {
        boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
        boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return (xlarge || large);
    }



}

