package com.safesurfer.database.serializers;

import com.frostnerd.database.orm.Serializer;
import com.safesurfer.database.entities.IPPortPair;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class IPPortSerializer extends Serializer<IPPortPair> {
    @Override
    protected String serializeValue(IPPortPair ipPortPair) {
        return ipPortPair.toString();
    }

    @Override
    public IPPortPair deserialize(String text) {
        return IPPortPair.wrap(text);
    }

    @Override
    public String serializeNull() {
        return "";
    }
}
