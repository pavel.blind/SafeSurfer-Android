package com.safesurfer.screens.events_history;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.safesurfer.R;
import com.safesurfer.screens.events_history.events_history_item.EventDetailsActivity;
import com.safesurfer.tensorflow.EventModel;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmResults;

public class EventsListActivityVC {

    RecyclerView recycler_view;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    private Context context;

    public EventsListActivityVC(Context context) {
        setContext(context);
    }

    private View getRootView(){
        return ((AppCompatActivity)getContext()).findViewById(android.R.id.content);
    }

    LinearLayoutManager manager;
    EventsAdapter adapter;

    public void setUpView(){
        recycler_view = getRootView().findViewById(R.id.recycler_view);
        manager = new LinearLayoutManager(getContext());


        Realm realm = Realm.getDefaultInstance();
        RealmResults<EventModel> list = realm.where(EventModel.class).findAll();

        adapter = new EventsAdapter( (OrderedRealmCollection)list, true, true);
        adapter.setContext(getContext());

        adapter.setListener(new EventsAdapter.EventsAdapterListener() {
            @Override
            public void onItemClick(EventModel model) {
                Intent intent = EventDetailsActivity.getIntent(getContext(), model.identifier );
                getContext().startActivity(intent);

                Animatoo.animateSlideLeft(context);
            }
        });

        recycler_view.setLayoutManager(manager);
        recycler_view.setAdapter(adapter);
    }

}
