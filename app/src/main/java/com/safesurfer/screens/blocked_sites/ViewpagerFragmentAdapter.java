package com.safesurfer.screens.blocked_sites;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;


import java.util.ArrayList;

public class ViewpagerFragmentAdapter extends FragmentStatePagerAdapter {

    ArrayList<Fragment> fragmentsList = new ArrayList<>();

    public ArrayList<Fragment> getFragmentsList() {
        return fragmentsList;
    }

    public void addItem(Fragment fragment) {
        this.fragmentsList.add(fragment);
    }

//    public void addItem(String url) {
//        ImagePageFragment fragment = ImagePageFragment.getInstance(url);
//        this.fragmentsList.add(fragment);
//    }
//
//    public void addItem(int imageRes) {
//        ImagePageFragment fragment = ImagePageFragment.getInstance(imageRes);
//        this.fragmentsList.add(fragment);
//    }

    /*==========================================================================*/
    /*==========================================================================*/
    /*==========================================================================*/

    public ViewpagerFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentsList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentsList.size();
    }


}
