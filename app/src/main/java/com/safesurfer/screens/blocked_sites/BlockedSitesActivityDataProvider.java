package com.safesurfer.screens.blocked_sites;

import android.content.Context;
import android.util.Log;

import com.safesurfer.BaseDataProvider;
import com.safesurfer.network_api.SafeSurferNetworkApi;
import com.safesurfer.network_api.entities.GetBlockedSitesResponse;
import com.safesurfer.util.Tools;

public class BlockedSitesActivityDataProvider extends BaseDataProvider {

    public BlockedSitesActivityDataProviderListener getListener() {
        return listener;
    }

    public void setListener(BlockedSitesActivityDataProviderListener listener) {
        this.listener = listener;
    }

    private BlockedSitesActivityDataProviderListener listener;

    public static interface BlockedSitesActivityDataProviderListener{
        public abstract void sitesListsReady(GetBlockedSitesResponse blockedSitesResponse);
        public abstract void requestError();
    }

    public BlockedSitesActivityDataProvider(Context context) {
        super(context);
    }

    @Override
    public Context getContext() {
        return super.getContext();
    }

    @Override
    public void setContext(Context context) {
        super.setContext(context);
    }


}
