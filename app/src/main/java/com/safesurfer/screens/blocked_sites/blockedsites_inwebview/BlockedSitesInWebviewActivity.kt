package com.safesurfer.screens.blocked_sites.blockedsites_inwebview

import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.safesurfer.R
import com.safesurfer.util.Tools
import lombok.`val`

class BlockedSitesInWebviewActivity : AppCompatActivity() {

    val URL_BLOCKED_SITES = "https://my.safesurfer.io/block-standalone"
    val mUIHandler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_blocked_sites_in_webview)

        supportActionBar?.hide()

        val mWebView = findViewById<WebView>(R.id.webview)

        mWebView.setFocusable(true);
        mWebView.setFocusableInTouchMode(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setDatabaseEnabled(true);
        mWebView.getSettings().setAppCacheEnabled(true);
        mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY)

        mWebView.setWebViewClient(object : WebViewClient() {

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)

                val key = "my-ss-token"

                val value = Tools.getInstance().clearedDeviceAuthToken
                mWebView.evaluateJavascript("localStorage.setItem('$key','$value');", null)
            }
        })
        mWebView?.loadUrl(URL_BLOCKED_SITES)
    }


}