package com.safesurfer.screens.blocked_sites;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.safesurfer.R;

import java.util.ArrayList;

public class SitesAdapter extends RecyclerView.Adapter<BlockedSitesActivityViewController.SiteViewHolder>{

    public SitesAdapter(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    Context context;
    ArrayList<String> sitesList = new ArrayList<>();

    public ArrayList<String> getSitesList() {
        return sitesList;
    }

    @NonNull
    @Override
    public BlockedSitesActivityViewController.SiteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //
        View itemView = ((AppCompatActivity) getContext()).getLayoutInflater().inflate(R.layout.siteslist_item_layout, null);
        itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return new BlockedSitesActivityViewController.SiteViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BlockedSitesActivityViewController.SiteViewHolder holder, int position) {
        holder.textview.setText( sitesList.get(position) );
    }

    @Override
    public int getItemCount() {
        return sitesList.size();
    }
}