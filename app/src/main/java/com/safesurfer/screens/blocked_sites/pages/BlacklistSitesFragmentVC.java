package com.safesurfer.screens.blocked_sites.pages;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.safesurfer.BaseFragmentViewController;
import com.safesurfer.R;
import com.safesurfer.network_api.entities.GetBlockedSitesResponse;
import com.safesurfer.screens.blocked_sites.SitesAdapter;
import com.safesurfer.util.Tools;

import static com.safesurfer.screens.blocked_sites.pages.BlacklistSitesFragment.BlacklistSitesFragment_BLACKLIST;
import static com.safesurfer.screens.blocked_sites.pages.BlacklistSitesFragment.BlacklistSitesFragment_WHITELIST;

public class BlacklistSitesFragmentVC extends BaseFragmentViewController {


    public BlacklistSitesFragmentVC(Fragment fragment) {
        super(fragment);
    }

    RecyclerView recyclerview_blacklist;
    LinearLayoutManager llmanager0;
    SitesAdapter blackListAdapter;

    public int getCurrentType() {
        return currentType;
    }

    public void setCurrentType(int currentType) {
        this.currentType = currentType;
    }

    private int currentType;

    public void setUpView(int type) {
        setCurrentType(type);
        setUpView();
    }

    @Override
    public void setUpView() {
        recyclerview_blacklist = getRootView().findViewById(R.id.recyclerview_blacklist);

        llmanager0 = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);

        blackListAdapter = new SitesAdapter(getActivity());

        recyclerview_blacklist.setLayoutManager(llmanager0);

        recyclerview_blacklist.setAdapter(blackListAdapter);

        blackListAdapter.notifyDataSetChanged();

        getRootView().findViewById(R.id.registerProgress).setVisibility(View.VISIBLE);
    }

    @Override
    public View getRootView() {
        return getFragment().getView();
    }

    public GetBlockedSitesResponse getBlockedSitesResponse() {
        return blockedSitesResponse;
    }

    public void setBlockedSitesResponse(GetBlockedSitesResponse blockedSitesResponse) {
        this.blockedSitesResponse = blockedSitesResponse;
    }

    GetBlockedSitesResponse blockedSitesResponse;

    public void sitesListsReady(GetBlockedSitesResponse blockedSitesResponse) {
        setBlockedSitesResponse(blockedSitesResponse);
        switch (getCurrentType()) {
            case BlacklistSitesFragment_WHITELIST:
                for (String site : blockedSitesResponse.whitelist) {
                    blackListAdapter.getSitesList().add(site);
                }
                blackListAdapter.notifyDataSetChanged();

                ((TextView) getRootView().findViewById(R.id.recyclerview_label)).setText(R.string.whitelist_sites);

                if (blockedSitesResponse.whitelist.size() == 0) {
                    getRootView().findViewById(R.id.recyclerview_label).setVisibility(View.VISIBLE);
                    ((TextView) getRootView().findViewById(R.id.recyclerview_label)).setText(R.string.list_is_empty);
                }else{
                    getRootView().findViewById(R.id.recyclerview_label).setVisibility(View.GONE);
                }
                break;
            case BlacklistSitesFragment_BLACKLIST:
                for (String site : blockedSitesResponse.blacklist) {
                    blackListAdapter.getSitesList().add(site);
                }
                blackListAdapter.notifyDataSetChanged();

                ((TextView) getRootView().findViewById(R.id.recyclerview_label)).setText(R.string.blacklist_sites);

                if (blockedSitesResponse.blacklist.size() == 0) {
                    getRootView().findViewById(R.id.recyclerview_label).setVisibility(View.VISIBLE);
                    ((TextView) getRootView().findViewById(R.id.recyclerview_label)).setText(R.string.list_is_empty);
                }else{
                    getRootView().findViewById(R.id.recyclerview_label).setVisibility(View.GONE);
                }
                break;
        }
        getRootView().findViewById(R.id.registerProgress).setVisibility(View.GONE);

    }

    public void onButtonAddClick() {
        switch (getCurrentType()) {
            case BlacklistSitesFragment_WHITELIST:
                dialogAddWhiteSiteList(getBlockedSitesResponse());
                break;
            case BlacklistSitesFragment_BLACKLIST:
                dialogAddBlackSiteList(getBlockedSitesResponse());
                break;
        }
    }

    public void requestError() {
        getRootView().findViewById(R.id.registerProgress).setVisibility(View.GONE);

        AlertDialog.Builder bld = new AlertDialog.Builder(getActivity());
        bld.setMessage(R.string.string_network_error);
        bld.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getActivity().finish();
            }
        });
        bld.create().show();
    }

    /*===================================================*/

    AlertDialog dialog;

    public void dialogAddWhiteSiteList(GetBlockedSitesResponse blockedSitesResponse) {
        // create new pin
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add site to whitelist");


        View inputView = ((AppCompatActivity) getContext()).getLayoutInflater().inflate(R.layout.dialog_enter_site, null);
        EditText edittext = inputView.findViewById(R.id.edittext);
        builder.setView(inputView);
        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String url = edittext.getText().toString();
                getListener().requestAddSiteToWhitelist(blockedSitesResponse, url);
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });

        dialog = builder.create();
        dialog.show();

        Tools.getInstance().showKeyboard(edittext);
        edittext.requestFocus();
    }

    public void dialogAddBlackSiteList(GetBlockedSitesResponse blockedSitesResponse) {
        // create new pin
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add site to blacklist");

        View inputView = ((AppCompatActivity) getContext()).getLayoutInflater().inflate(R.layout.dialog_enter_site, null);
        EditText edittext = inputView.findViewById(R.id.edittext);
        builder.setView(inputView);
        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String url = edittext.getText().toString();
                getListener().requestAddSiteToBlacklist(blockedSitesResponse, url);
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });

        dialog = builder.create();
        dialog.show();

        Tools.getInstance().showKeyboard(edittext);
        edittext.requestFocus();
    }

    public BlacklistSitesFragmentVCListener getListener() {
        return listener;
    }

    public void setListener(BlacklistSitesFragmentVCListener listener) {
        this.listener = listener;
    }

    private BlacklistSitesFragmentVCListener listener;

    public static interface BlacklistSitesFragmentVCListener {
        public abstract void requestAddSiteToWhitelist(GetBlockedSitesResponse blockedSitesResponse, String url);

        public abstract void requestAddSiteToBlacklist(GetBlockedSitesResponse blockedSitesResponse, String url);
    }

}
