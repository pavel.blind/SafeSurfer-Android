package com.safesurfer.screens.blocked_sites.pages;

import android.content.Context;
import android.util.Log;

import com.safesurfer.BaseDataProvider;
import com.safesurfer.network_api.SafeSurferNetworkApi;
import com.safesurfer.network_api.entities.GetBlockedSitesResponse;
import com.safesurfer.util.Tools;

public class BlacklistSitesFragmentDP extends BaseDataProvider {

    public static interface BlacklistSitesFragmentDPListener {
        public abstract void sitesListsReady(GetBlockedSitesResponse blockedSitesResponse);

        public abstract void blockedSiteAddedSuccessfully();

        public abstract void requestError();
    }

    public BlacklistSitesFragmentDPListener getListener() {
        return listener;
    }

    public void setListener(BlacklistSitesFragmentDPListener listener) {
        this.listener = listener;
    }

    private BlacklistSitesFragmentDPListener listener;

    public BlacklistSitesFragmentDP(Context context) {
        super(context);
    }

    public void addSiteToWhitelist(GetBlockedSitesResponse blockedSitesResponse, String url) {
        String authDeviceToken = Tools.getInstance().getDeviceAuthToken();
        SafeSurferNetworkApi.getInstance()
                .getSRUseCase()
                .setBlockedSitesWhiteList(blockedSitesResponse, authDeviceToken, url)
                .toObservable()
                .subscribe(success -> {
                   if( success.code() == 200){
                       getListener().blockedSiteAddedSuccessfully();
                   }
                }, fail -> {
                    fail.printStackTrace();

                    getListener().requestError();
                });
    }

    public void addSiteToBlacklist(GetBlockedSitesResponse blockedSitesResponse, String url) {
        String authDeviceToken = Tools.getInstance().getDeviceAuthToken();
        SafeSurferNetworkApi.getInstance()
                .getSRUseCase()
                .setBlockedSitesBlackList(blockedSitesResponse, authDeviceToken, url)
                .toObservable()
                .subscribe(success -> {
                    if( success.code() == 200){
                        getListener().blockedSiteAddedSuccessfully();
                    }
                }, fail -> {
                    fail.printStackTrace();

                    getListener().requestError();
                });
    }

    public void getBlockedSites() {

        String authDeviceToken = Tools.getInstance().getDeviceAuthToken();
        SafeSurferNetworkApi.getInstance()
                .getSRUseCase()
                .getBlockedSites(authDeviceToken)
                .toObservable()
                .subscribe(success -> {
                    GetBlockedSitesResponse blockedSitesResponse = success.body();
//                    blockedSitesResponse.

                    Log.d(getClass().getName(), "blockedSitesResponse");

                    getListener().sitesListsReady(blockedSitesResponse);
                }, fail -> {
                    fail.printStackTrace();

                    getListener().requestError();
                });
    }
}
