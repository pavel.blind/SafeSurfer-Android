package com.safesurfer.screens.screencast_settings.presets;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.safesurfer.R;
import com.safesurfer.util.ThemeHandler;
import com.safesurfer.util.Tools;

import static com.safesurfer.util.Tools.LEVEL_EXPLICIT;
import static com.safesurfer.util.Tools.LEVEL_NEUTRAL;
import static com.safesurfer.util.Tools.LEVEL_SUGGESTIVE;

public class ScreenCastPresetsActivity extends AppCompatActivity {

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(ThemeHandler.getPreferenceTheme(this));
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_screen_cast_presets);

        findViewById(R.id.clickable).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Tools.getInstance().saveCastSettingValue(LEVEL_EXPLICIT, 30 );
                Tools.getInstance().saveCastSettingValue(LEVEL_SUGGESTIVE, 40 );
                Tools.getInstance().saveCastSettingValue(LEVEL_NEUTRAL, 50);

                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            Animatoo.animateSlideRight(context);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        Animatoo.animateSlideRight(context);
    }

}
