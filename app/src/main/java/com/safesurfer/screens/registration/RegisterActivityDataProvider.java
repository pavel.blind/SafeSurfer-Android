package com.safesurfer.screens.registration;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import com.safesurfer.BaseDataProvider;
import com.safesurfer.R;
import com.safesurfer.network_api.SafeSurferNetworkApi;
import com.safesurfer.util.Constants;
import com.safesurfer.util.Tools;

import static android.content.Context.MODE_PRIVATE;
import static com.safesurfer.util.Constants.REGISTRATION_PREFERENCES_KEY_AUTH_DEVICE_TOKEN;
import static com.safesurfer.util.Constants.REGISTRATION_PREFERENCES_KEY_AUTH_TOKEN;
import static com.safesurfer.util.Constants.REGISTRATION_PREFERENCES_KEY_DEVICEID;

public class RegisterActivityDataProvider extends BaseDataProvider {

    public RegisterActivityDataProvider(Context context) {
        super(context);
    }

    public static interface RegisterActivityDataProviderListener {
        public abstract void onSuccess(String email, String password, String secureID, String mac);

        public abstract void onError(String errorMessage);
    }

    public RegisterActivityDataProviderListener getListener() {
        if (listener == null) return new RegisterActivityDataProviderListener() {
            @Override
            public void onSuccess(String email, String password, String secureID, String mac) {

            }

            @Override
            public void onError(String errorMessage) {

            }
        };
        return listener;
    }

    public void setListener(RegisterActivityDataProviderListener listener) {
        this.listener = listener;
    }

    //        SafeSurferNetworkApi.getInstance().getSRUseCase()
    //                .deviceAuth(email, password, mac)
    //                .toObservable()
    //                .subscribe(success -> {
    //
    //                }, fail -> {
    //                    fail.printStackTrace();
    //                });

    //                    success.string()
                            /*Returns a JSON string depending on success:
                            "success" if everything is good,
                            "duplicate" if a user with the email already exists,
                            "error" for an unkown error.
                            The user will be sent a verification email.*/

    public String getSecureId() {
        return mSecureId;
    }

    public void setSecureId(String mSecureId) {
        this.mSecureId = mSecureId;
    }

    public String getMac() {
        return mMac;
    }

    public void setMac(String mMac) {
        this.mMac = mMac;
    }

    private RegisterActivityDataProviderListener listener;

//    public static final String REGISTER_RESPONSE_SUCCESS = "\"success\"";
//    public static final String REGISTER_RESPONSE_DUPLICATE = "\"duplicate\"";
//    public static final String REGISTER_RESPONSE_ERROR = "\"error\"";

    String mSecureId;
    String mMac;

    public static final String REGISTER_RESPONSE_SUCCESS = "\"success\"";
    public static final String REGISTER_RESPONSE_DUPLICATE = "\"duplicate\"";
    public static final String REGISTER_RESPONSE_ERROR = "\"error\"";

    public void requestRegister(final String email, final String password, String secureId, String mac) {

        setSecureId(secureId);
        setMac(mac);

        SafeSurferNetworkApi.getInstance().getSRUseCase()
                .createNewUser(email, password)
                .subscribe(response -> {
                    String message;
                    Log.d(getClass().getName(), String.format("code %s", response.code()));
                    switch (response.code()) {
                        case 200:
                            message = response.body().string();
                            if(message.equals("")){
                                message = response.message();
                            }
                            Log.d(getClass().getName(), message);

                            if (message.equals( REGISTER_RESPONSE_SUCCESS)) {
                                Log.d(getClass().getName(), "if (message.equals(REGISTER_RESPONSE_SUCCESS)");
                                login(email, password);
                            }
                            break;
                        case 400:
                            message = response.errorBody().string();
                            if (message.equals( REGISTER_RESPONSE_DUPLICATE)) {
                                Log.d(getClass().getName(), "if (message.equals(REGISTER_RESPONSE_DUPLICATE)");
                                login(email, password);
                            }
                            if (message.equals( REGISTER_RESPONSE_ERROR)) {
                                Log.d(getClass().getName(), "if (message.equals(REGISTER_RESPONSE_ERROR)");
                                getListener().onError(getContext().getString(R.string.dataprovider_authentication_error));
                            }
                            break;
                        default:
                            getListener().onError(getContext().getString(R.string.dataprovider_authentication_error));
                            break;
                    }
                }, fail -> {
                    fail.printStackTrace();
                    getListener().onError(getContext().getString(R.string.dataprovider_authentication_error));
                });
    }

    public void login(final String email, final String password) {

        SafeSurferNetworkApi.getInstance().getSRUseCase()
                .logIn(email, password, true)
                .toObservable()
                .subscribe(success -> {

                    int code = success.code();

                    switch (code) {
                        case 200:
                            Tools.getInstance().saveCurrentUserLogin( email);
                            String authToken = success.body().token;

                            SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
                            preferences.edit().putString(REGISTRATION_PREFERENCES_KEY_AUTH_TOKEN, authToken).commit();
                            getDeviceID(email, password);
                            break;
                        default:
                            getListener().onError(getContext().getString(R.string.dataprovider_authentication_error) );
                            break;
                    }

                }, fail -> {
                    getListener().onError(getContext().getString(R.string.dataprovider_authentication_error));
                    fail.printStackTrace();
                });
    }

    public void getDeviceID(final String email, final String password) {
        String model = Build.MODEL;
        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        String authToken = "Bearer " + preferences.getString(REGISTRATION_PREFERENCES_KEY_AUTH_TOKEN, null);
        if(!preferences.getString(REGISTRATION_PREFERENCES_KEY_DEVICEID, "").equals("")){
            deviceAuth(email, password);
            return;
        }
        SafeSurferNetworkApi.getInstance()
                .getSRUseCase()
                .registerDevice(authToken, "mobile", model)
                .subscribe(response -> {
                    switch (response.code()) {
                        case 200:
                            String deviceId = response.body().string();
                            deviceId = deviceId.replaceAll("^\"|\"$", "");
                            preferences.edit().putString(REGISTRATION_PREFERENCES_KEY_DEVICEID, deviceId).commit();

                            deviceAuth(email, password);
                            break;
                        case 400:
                            getListener().onError(getContext().getString(R.string.dataprovider_authentication_error));
                            break;
                        case 500:
                            getListener().onError(getContext().getString(R.string.dataprovider_authentication_error));
                            break;
                        default:
                            getListener().onError(getContext().getString(R.string.dataprovider_authentication_error));
                            break;
                    }
                }, fail -> {
                    getListener().onError(getContext().getString(R.string.dataprovider_authentication_error));
                    fail.printStackTrace();
                });
    }

    public void deviceAuth(String email, String password) {

        SharedPreferences preferences = getContext().getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        String authToken = "Bearer " + preferences.getString(REGISTRATION_PREFERENCES_KEY_AUTH_TOKEN, null);
        String deviceId = preferences.getString(REGISTRATION_PREFERENCES_KEY_DEVICEID, null);

        SafeSurferNetworkApi.getInstance().getSRUseCase()
                .deviceAuth(authToken, deviceId, email, password, true)
                .toObservable()
                .subscribe(success -> {
                    switch (success.code()){
                        case 200:
                            String deviceAuthToken = success.body().token;
                            Tools.getInstance().setDeviceAuthToken(deviceAuthToken);

                            getListener().onSuccess(email, password, getSecureId(), getMac());
                            break;
                        default:
                            getListener().onError(getContext().getString(R.string.dataprovider_authentication_error));
                            break;
                    }
                }, fail -> {
                    getListener().onError(getContext().getString(R.string.dataprovider_authentication_error));
                    fail.printStackTrace();
                });
    }

}
