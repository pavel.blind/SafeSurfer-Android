package com.safesurfer.screens.reset_pin;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import com.safesurfer.BaseViewController;
import com.safesurfer.R;
import com.safesurfer.screens.EnterPinActivity;
import com.safesurfer.util.Tools;

public class ResetPinAuthActivityViewController extends BaseViewController {

    public ResetPinAuthActivityViewController(Context context) {
        super(context);
    }

    EditText textview_email;
    EditText textview_password;
    View cancel;
    View authenticate;

    @Override
    public void setUpView() {
        textview_email = (EditText) getRootView().findViewById(R.id.email);
        textview_password = (EditText) getRootView().findViewById(R.id.password);

        cancel = getRootView().findViewById(R.id.cancel);
        authenticate = getRootView().findViewById(R.id.authenticate);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });

        authenticate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = textview_email.getText().toString();

                String currentUserLogin= Tools.getInstance().getCurrentUserLogin();
                if(!email.equals(currentUserLogin)){
                    showError( getActivity().getString(R.string.entered_wrong_login) );
                    return;
                }

                String password = textview_password.getText().toString();
                if( !email.equals("") & !password.equals("") ){
                    getListener().requestAuthentication(email, password);
                }
            }
        });
    }

    public ResetPinAuthActivityViewControllerListener getListener() {
        return listener;
    }

    public void setListener(ResetPinAuthActivityViewControllerListener listener) {
        this.listener = listener;
    }

    ResetPinAuthActivityViewControllerListener listener;

    public static interface ResetPinAuthActivityViewControllerListener {
        public abstract void requestAuthentication(String email, String password);
    }

    AlertDialog dialog;

    public void onSuccessAuthenticate() {
        // create new pin
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Enter new pin");

//        EditText edittext = new EditText(getContext());
//        edittext.setInputType(InputType.TYPE_CLASS_NUMBER );
//        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams( LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
//        edittext.setLayoutParams(lp);
        View inputView = ((AppCompatActivity) getContext()).getLayoutInflater().inflate(R.layout.dialog_reset_pincode, null);
        EditText editTextNewPin = inputView.findViewById(R.id.edittext);
        builder.setView(inputView);
        builder.setPositiveButton("Change pin", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String newPinCode = editTextNewPin.getText().toString();
                saveNewPinCode(newPinCode);
                dialog.dismiss();

                getActivity().startActivity(new Intent(getActivity(), EnterPinActivity.class));
                getActivity().finish();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });

        dialog = builder.create();
        dialog.show();

        Tools.getInstance().showKeyboard(editTextNewPin);
        editTextNewPin.requestFocus();
    }

    private void saveNewPinCode(String pinCode) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        String oldPinCode = prefs.getString("pin_value", null);
        Log.d(getClass().getName(), "oldPinCode " + String.valueOf(oldPinCode));
        Log.d(getClass().getName(), "new pinCode " + String.valueOf(pinCode));

        prefs.edit().putString("pin_value", pinCode).commit();
    }

    public void onFailAuthenticate() {
        Toast.makeText(getActivity(), R.string.authentication_error_string, Toast.LENGTH_SHORT).show();
    }

    @Override
    public View getRootView() {
        return ((AppCompatActivity) getActivity()).findViewById(android.R.id.content);
    }

    public void showError(String message) {

        AlertDialog.Builder bld = new AlertDialog.Builder(getActivity());
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
        bld.create().show();
    }
}
