package com.safesurfer.screens;

import android.app.SearchManager;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.collection.ArraySet;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;

import com.safesurfer.network_api.SafeSurferNetworkApi;
import com.safesurfer.screens.blocked_sites.blockedsites_inwebview.BlockedSitesInWebviewActivity;
import com.safesurfer.screens.registration.RegisterActivity;
import com.safesurfer.database.entities.IPPortPair;

import com.safesurfer.fragments.MainFragment;
import com.safesurfer.fragments.QueryLogFragment;
import com.safesurfer.fragments.SettingsFragment;
import com.safesurfer.services.DNSVpnService;

import android.provider.Settings;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.frostnerd.design.DesignUtil;

import com.frostnerd.design.dialogs.LoadingDialog;
import com.frostnerd.design.navigationdrawer.DrawerItem;
import com.frostnerd.design.navigationdrawer.DrawerItemCreator;
import com.frostnerd.design.navigationdrawer.NavigationDrawerActivity;
import com.frostnerd.design.navigationdrawer.StyleOptions;
import com.safesurfer.BuildConfig;
import com.safesurfer.LogFactory;
import com.safesurfer.R;

import com.safesurfer.services.PingService;
import com.safesurfer.util.libscreenshotter.scheduler.ScreenCapturerActivity;
import com.safesurfer.util.libscreenshotter.scheduler.ScreenCapturerService;
import com.safesurfer.util.Constants;
import com.safesurfer.util.PreferencesAccessor;

import com.safesurfer.util.ThemeHandler;
import com.safesurfer.util.Tools;
import com.safesurfer.util.Util;
import com.safesurfer.util.Preferences;
import com.frostnerd.general.Utils;

import com.google.android.material.snackbar.Snackbar;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 * <p>
 * This file is part of SafeSurfer-Android.
 * <p>
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class MainActivity extends NavigationDrawerActivity {
    private static final String LOG_TAG = "[MainActivity]";
    private static final int REQUEST_PERMISSION_IMPORT_SETTINGS = 131, REQUEST_PERMISSION_EXPORT_SETTINGS = 130;
    private AlertDialog dialog1;
    private AlertDialog dialog2;

    private MainFragment mainFragment;
    private SettingsFragment settingsFragment;
    private DrawerItem defaultDrawerItem, settingsDrawerItem;
    @ColorInt
    private int backgroundColor;
    @ColorInt
    private int textColor, navDrawableColor;
    private boolean startedActivity = false, importingRules = false;
    private final BroadcastReceiver shortcutReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final Snackbar snackbar = Snackbar.make(getContentFrame(), R.string.shortcut_created, Snackbar.LENGTH_INDEFINITE);
            snackbar.setAction(R.string.show, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbar.dismiss();
                    Utils.goToLauncher(MainActivity.this);
                }
            });
            snackbar.show();
        }
    };
    private BroadcastReceiver importFinishedReceiver;

    SharedPreferences sharedPreferences;
    SharedPreferences _sp_registration;

    @Override
    protected void onResume() {
        super.onResume();
        startedActivity = false;



        // beginPingUpdates();

        // Receiver is not unregistered in onPause() because the app is in the background when a shortcut
        // is created

        /* Safe Surfer Ported Functionality to check if user is registered and if so do the 4 min poll.
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation);
        if (navigationView != null)
        {
            SharedPreferences preferences = getSharedPreferences(Constant.REGISTRATION_PREFERENCES, MODE_PRIVATE);
            String udid = preferences.getString(Constant.REGISTRATION_PREFERENCES_KEY_UDID, null);

            if (udid == null)
            {
                // Not Registered...
                navigationView.getMenu().findItem(R.id.drawer_navigation_register).setVisible(true);
                navigationView.getMenu().findItem(R.id.drawer_navigation_restrict_sites_of_concern).setVisible(false);
            }
            else
            {
                // Registered...
                navigationView.getMenu().findItem(R.id.drawer_navigation_register).setVisible(false);
                navigationView.getMenu().findItem(R.id.drawer_navigation_restrict_sites_of_concern).setVisible(true);

                beginPingUpdates();

            }

        }
        */


    }


    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    private Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        try {
            setTheme(ThemeHandler.getAppTheme(this));
            setContext(this);

            backgroundColor = ThemeHandler.resolveThemeAttribute(getTheme(), android.R.attr.colorBackground);
            textColor = ThemeHandler.resolveThemeAttribute(getTheme(), android.R.attr.textColor);
            navDrawableColor = ThemeHandler.resolveThemeAttribute(getTheme(), R.attr.navDrawableColor);
            super.onCreate(savedInstanceState);
            Util.runBackgroundConnectivityCheck(this, true);
            final Preferences preferences = Preferences.getInstance(this);

            if (preferences.getBoolean("first_run", true))
                preferences.put("setting_show_notification", false);
            if (preferences.getBoolean("first_run", true))
                preferences.put("setting_start_boot", true);
            if (preferences.getBoolean("first_run", true))
                preferences.put("setting_auto_wifi", true);
            if (preferences.getBoolean("first_run", true))
                preferences.put("excluded_apps", new ArraySet<>(Arrays.asList(getResources().getStringArray(R.array.default_blacklist))));
            if (preferences.getBoolean("first_run", true))
                preferences.put("setting_auto_mobile", true);
            if (preferences.getBoolean("first_run", true))
                preferences.put("check_connectivity", true);
            if (preferences.getBoolean("first_run", true) && Util.isTaskerInstalled(this)) {
                LogFactory.writeMessage(this, LOG_TAG, "Showing dialog telling the user that this app supports Tasker");
                new AlertDialog.Builder(this, ThemeHandler.getDialogTheme(this)).setTitle(R.string.tasker_support).setMessage(R.string.app_supports_tasker_text).setPositiveButton(R.string.got_it, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).show();
                LogFactory.writeMessage(this, LOG_TAG, "Dialog is now being shown");
            }
            int random = new Random().nextInt(100), launches = preferences.getInteger("launches", 0);
            preferences.put("launches", launches + 1);
            if (launches >= 5 && !preferences.getBoolean("first_run", true) &&
                    !preferences.getBoolean("rated", false) && random <= 16) {
                LogFactory.writeMessage(this, LOG_TAG, "Showing dialog requesting rating");
                new AlertDialog.Builder(this, ThemeHandler.getDialogTheme(this)).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        rateApp();
                    }
                }).setNegativeButton(R.string.dont_ask_again, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        preferences.put("rated", true);
                        dialog.cancel();
                    }
                }).setNeutralButton(R.string.not_now, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).setMessage(R.string.rate_request_text).setTitle(R.string.rate).show();
                LogFactory.writeMessage(this, LOG_TAG, "Dialog is now being shown");
            }
        } catch (Exception ex) {
            LogFactory.writeMessage(this, LOG_TAG, ex.toString());
        }

        _sp_registration = getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
        if (_sp_registration.getString(Constants.REGISTRATION_PREFERENCES_KEY_EMAIL, "") != null && _sp_registration.getString(Constants.REGISTRATION_PREFERENCES_KEY_EMAIL, "").trim().length() > 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                beginPingUpdates();
            }
        }

        // This block helps to disable all the functionalists.


        //Util.updateTiles(this);
       /* View cardView = getLayoutInflater().inflate(R.layout.main_cardview, null, false);
        final TextView text = cardView.findViewById(R.id.text);
        final Switch button = cardView.findViewById(R.id.cardview_switch);
        if(PreferencesAccessor.isEverythingDisabled(this)){
            button.setChecked(true);
            text.setText(R.string.cardview_text_disabled);
        }
        button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                LogFactory.writeMessage(MainActivity.this, new String[]{LOG_TAG, "[DISABLE-EVERYTHING]"}, "The DisableEverything switch was clicked and changed to " + b);
                text.setText(b ? R.string.cardview_text_disabled : R.string.cardview_text);
                preferences.put("everything_disabled", b);
                if(Util.isServiceRunning(MainActivity.this)){
                    LogFactory.writeMessage(MainActivity.this, new String[]{LOG_TAG, "[DISABLE-EVERYTHING]"}, "Service is running. Destroying...");
                    startService(DNSVpnService.getDestroyIntent(MainActivity.this));
                }
            }
        });
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button.toggle();
            }
        });
        setCardView(cardView);
        preferences.put( "first_run", false);*/

        if (Tools.getInstance().isMyServiceRunning(ScreenCapturerService.class)) {
            Log.d(getClass().getName(), "ScreenCapturerService is running");
        } else {
            Log.d(getClass().getName(), "ScreenCapturerService is not running");

            if (Tools.getInstance().isScreencastDetectionActive()) {
                Log.d(getClass().getName(), "run ScreenCapturerService");
                Intent intent = new Intent(Tools.getInstance().getContext(), ScreenCapturerActivity.class);
                startActivity(intent);
            }
        }

        if (Tools.getSdkVersion() >= 23) {
            try {
                if (!Settings.canDrawOverlays(this)) {
                    // ask for setting
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, REQUEST_OVERLAY_PERMISSION);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        checkDeviceJWT();
    }


    final int REQUEST_OVERLAY_PERMISSION = 100;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_OVERLAY_PERMISSION) {
            try {
                if (Tools.getSdkVersion() >= 23) {
                    if (Settings.canDrawOverlays(this)) {

                    } else {

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (getCurrentFragment() == settingsFragment) {
            getMenuInflater().inflate(R.menu.menu_settings, menu);

            SearchManager searchManager = Utils.requireNonNull((SearchManager) getSystemService(Context.SEARCH_SERVICE));
            SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
            searchView.setOnQueryTextListener(settingsFragment);
            return true;
        } else return super.onCreateOptionsMenu(menu);
    }

    @Override
    public SharedPreferences getSharedPreferences(String name, int mode) {
        if (name == "com.safesurfer.registration") {
            return super.getSharedPreferences(name, mode);
        }
        return Preferences.getInstance(this);
    }

    @Override
    protected void onDestroy() {
        if (dialog1 != null && dialog1.isShowing()) dialog1.cancel();
        if (importFinishedReceiver != null) unregisterReceiver(importFinishedReceiver);
        super.onDestroy();
    }

    @Override
    protected Configuration getConfiguration() {
        return Configuration.withDefaults().setDismissFragmentsOnPause(false);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean handled = false;
        switch (keyCode) {
            case KeyEvent.KEYCODE_MEDIA_PLAY:
            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                if (currentFragment() instanceof MainFragment) {
                    handled = true;
                    ((MainFragment) currentFragment()).toggleVPN();
                }
                break;
        }
        return handled || super.onKeyDown(keyCode, event);
    }

    @NonNull
    @Override
    public DrawerItem getDefaultItem() {
        return defaultDrawerItem;
    }

    @Override
    public void onItemClicked(DrawerItem item, boolean handle) {
    }

    public Fragment currentFragment() {
        return getCurrentFragment();
    }

    public static final String URL_BLOCKED_SITES = "https://my.safesurfer.io/block-standalone";

    @Override
    public List<DrawerItem> createDrawerItems() {
        DrawerItemCreator itemCreator = new DrawerItemCreator(this);
//        itemCreator.createItemAndContinue(R.string.nav_title_main);
        //R.string.nav_title_dns replaced to string.Empty
        //cannot change the title in NavigationDrawerActivity because its a ready only file.
        //Changing the title in NavigationDrawerActivity is the only possible way
        itemCreator.createItemAndContinue(context.getString(R.string.nav_title_home), setDrawableColor(DesignUtil.getDrawable(this, R.drawable.ic_home)), new DrawerItem.FragmentCreator() {
            @Override
            public Fragment getFragment(@Nullable Bundle arguments) {
                return mainFragment = new MainFragment();
            }
        }).accessLastItemAndContinue(new DrawerItemCreator.ItemAccessor() {
            @Override
            public void access(DrawerItem item) {
                defaultDrawerItem = item;
                item.setRecreateFragmentOnConfigChange(true);
            }
        });
        itemCreator.createItemAndContinue(R.string.settings, setDrawableColor(DesignUtil.getDrawable(this, R.drawable.ic_settings)), new DrawerItem.FragmentCreator() {
            @Override
            public Fragment getFragment(@Nullable Bundle arguments) {
                settingsFragment = new SettingsFragment();
                if (arguments != null) settingsFragment.setArguments(arguments);
                return settingsFragment;
            }
        }).accessLastItemAndContinue(new DrawerItemCreator.ItemAccessor() {
            @Override
            public void access(DrawerItem item) {
                settingsDrawerItem = item;
                item.setInvalidateActivityMenu(true);
            }
        });

        _sp_registration = getSharedPreferences(Constants.REGISTRATION_PREFERENCES, MODE_PRIVATE);
//        if (_sp_registration.getString(Constants.REGISTRATION_PREFERENCES_KEY_EMAIL, "").trim().length() > 0) {
        if ( Tools.getInstance().getDeviceAuthToken() != null) {
            itemCreator.createItemAndContinue(R.string.nav_title_blocked_sites, setDrawableColor(DesignUtil.getDrawable(this, R.drawable.ic_action_key)), new DrawerItem.ClickListener() {
                @Override
                public boolean onClick(DrawerItem item, NavigationDrawerActivity drawerActivity, @Nullable Bundle arguments) {
                    Intent mainActivityIntent = new Intent(MainActivity.this, BlockedSitesInWebviewActivity.class);
                    startActivity(mainActivityIntent);

//                    Intent intent = new Intent(Intent.ACTION_VIEW);
//                    intent.setData( Uri.parse( URL_BLOCKED_SITES ));
//                    getContext().startActivity(intent);

                    return false;
                }

                @Override
                public boolean onLongClick(DrawerItem item, NavigationDrawerActivity drawerActivity) {
                    return false;
                }
            });
        } else {
            itemCreator.createItemAndContinue(R.string.register, setDrawableColor(DesignUtil.getDrawable(this, R.drawable.ic_register)), new DrawerItem.ClickListener() {
                @Override
                public boolean onClick(DrawerItem item, NavigationDrawerActivity drawerActivity, @Nullable Bundle arguments) {
                    Intent RegisterIntent = new Intent(MainActivity.this, RegisterActivity.class);
                    startActivity(RegisterIntent);
                    finish();
                    return false;
                }

                @Override
                public boolean onLongClick(DrawerItem item, NavigationDrawerActivity drawerActivity) {
                    return false;
                }
            });
        }


        if (PreferencesAccessor.isAdvancedModeEnabled(this)) {
            itemCreator.createItemAndContinue(R.string.nav_title_advanced);
            itemCreator.createItemAndContinue(R.string.title_advanced_settings, setDrawableColor(DesignUtil.getDrawable(this, R.drawable.ic_settings)), new DrawerItem.ClickListener() {
                @Override
                public boolean onClick(DrawerItem item, NavigationDrawerActivity drawerActivity, @Nullable Bundle arguments) {
                    startActivity(new Intent(MainActivity.this, AdvancedSettingsActivity.class));
                    return false;
                }

                @Override
                public boolean onLongClick(DrawerItem item, NavigationDrawerActivity drawerActivity) {
                    return false;
                }
            });
            if (PreferencesAccessor.isQueryLoggingEnabled(this)) {
                itemCreator.createItemAndContinue(R.string.nav_title_query_log, setDrawableColor(DesignUtil.getDrawable(this, R.drawable.ic_timelapse)), new DrawerItem.FragmentCreator() {
                    @Override
                    public Fragment getFragment(@Nullable Bundle arguments) {
                        return new QueryLogFragment();
                    }
                }).accessLastItemAndContinue(new DrawerItemCreator.ItemAccessor() {
                    @Override
                    public void access(DrawerItem item) {
                        item.setRecreateFragmentOnConfigChange(true);
                    }
                });
            }
        }
        itemCreator.createItemAndContinue(R.string.nav_title_features);
        itemCreator.createItemAndContinue(R.string.nav_title_pin_protection, setDrawableColor(DesignUtil.getDrawable(this, R.drawable.ic_action_key)), new DrawerItem.ClickListener() {
            @Override
            public boolean onClick(DrawerItem item, NavigationDrawerActivity drawerActivity, @Nullable Bundle arguments) {
                dialog1 = new AlertDialog.Builder(MainActivity.this).setTitle(item.getTitle()).setNegativeButton(R.string.close, null)
                        .setPositiveButton("pos", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                openSettingsAndScrollToKey("pin_category");
                            }
                        }).setMessage(R.string.feature_pin_protection).create();
                dialog1.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        Button button = dialog1.getButton(android.app.AlertDialog.BUTTON_POSITIVE);
                        button.setText("");
                        Drawable gear = setDrawableColor(getResources().getDrawable(R.drawable.ic_settings));
                        button.setCompoundDrawablesWithIntrinsicBounds(gear, null, null, null);
                    }
                });
                dialog1.show();
                return false;
            }

            @Override
            public boolean onLongClick(DrawerItem item, NavigationDrawerActivity drawerActivity) {
                return false;
            }
        });

        int iconId = R.drawable.ic_launched_watching;

//        if (Tools.getInstance().isScreencastDetectionActive()) {
//            switch (Tools.getInstance().getScreencastMode()) {
//                case Tools.SCREENCAST_MODE_ALWAYS:
//                    iconId = R.drawable.ic_launched_always;
//                    break;
//                case Tools.SCREENCAST_MODE_WATCHING:
//                    iconId = R.drawable.ic_launched_watching;
//                    break;
//            }
//        } else {
//            iconId = R.drawable.ic_not_running;
//        }

        itemCreator
                .createItemAndContinue(R.string.nav_title_screencast_detection, setDrawableColor(DesignUtil.getDrawable(this, iconId)),
                        new DrawerItem.ClickListener() {
                            @Override
                            public boolean onClick(DrawerItem item, NavigationDrawerActivity drawerActivity, @Nullable Bundle arguments) {
                                createScreencastDetectionDialog();
                                return false;
                            }

                            @Override
                            public boolean onLongClick(DrawerItem item, NavigationDrawerActivity drawerActivity) {
                                return false;
                            }
                        });
        itemCreator.createItemAndContinue(R.string.app_name);
        itemCreator.createItemAndContinue(R.string.rate, setDrawableColor(DesignUtil.getDrawable(this, R.drawable.ic_star_white)), new DrawerItem.ClickListener() {
            @Override
            public boolean onClick(DrawerItem item, NavigationDrawerActivity drawerActivity, @Nullable Bundle arguments) {
                rateApp();
                return false;
            }

            @Override
            public boolean onLongClick(DrawerItem item, NavigationDrawerActivity drawerActivity) {
                return false;
            }
        });
        itemCreator.createItemAndContinue(R.string.title_share_app, setDrawableColor(DesignUtil.getDrawable(this, R.drawable.ic_share)), new DrawerItem.ClickListener() {
            @Override
            public boolean onClick(DrawerItem item, NavigationDrawerActivity drawerActivity, @Nullable Bundle arguments) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.app_share_text));
                LogFactory.writeMessage(MainActivity.this, LOG_TAG, "Showing chooser for share", sharingIntent);
                startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.share_using)));
                return false;
            }

            @Override
            public boolean onLongClick(DrawerItem item, NavigationDrawerActivity drawerActivity) {
                return false;
            }
        });
        itemCreator.createItemAndContinue(R.string.contact_developer, setDrawableColor(DesignUtil.getDrawable(this, R.drawable.ic_person)), new DrawerItem.ClickListener() {
            @Override
            public boolean onClick(DrawerItem item, NavigationDrawerActivity drawerActivity, @Nullable Bundle arguments) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "support@safesurfer.co.nz", null));
                String body = "\n\n\n\n\n\n\nSystem:\nApp version: " + BuildConfig.VERSION_CODE + " (" + BuildConfig.VERSION_NAME + ")\n" +
                        "Android: " + Build.VERSION.SDK_INT + " (" + Build.VERSION.RELEASE + " - " + Build.VERSION.CODENAME + ")";
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                emailIntent.putExtra(Intent.EXTRA_EMAIL, "support@safesurfer.co.nz");
                emailIntent.putExtra(Intent.EXTRA_TEXT, body);
                LogFactory.writeMessage(MainActivity.this, LOG_TAG, "Now showing chooser for contacting dev", emailIntent);
                startActivity(Intent.createChooser(emailIntent, getString(R.string.contact_developer)));
                return false;
            }

            @Override
            public boolean onLongClick(DrawerItem item, NavigationDrawerActivity drawerActivity) {
                return false;
            }
        });
        itemCreator.createItemAndContinue(R.string.nav_title_libraries, setDrawableColor(DesignUtil.getDrawable(this, R.drawable.ic_library_books)), new DrawerItem.ClickListener() {
            @Override
            public boolean onClick(DrawerItem item, NavigationDrawerActivity drawerActivity, @Nullable Bundle arguments) {
                String licenseText = "\n\n" + getString(R.string.dialog_libraries_text) + "";
                licenseText += "\n\n- - - - - - - - - - - -\nDNSChanger v6 Copyright Daniel Wolf 2017\n\nGNU General Public License v3.0 with exceptions granted.";
                licenseText += "\n\n- - - - - - - - - - - -\ndnsjava by Brian Wellington - http://www.xbill.org/dnsjava/\n\n" + getString(R.string.license_bsd_2).replace("[yearrange]", "1998-2011").replace("[author]", "Brian Wellington");
                licenseText += "\n\n- - - - - - - - - - - -\nfirebase-jobdispatcher-android by Google\n\nAvailable under the [1]Apache License 2.0[2]";
                licenseText += "\n\n- - - - - - - - - - - -\npcap4j by Kaito Yamada\n\nAvailable under the [3]MIT License[4]";
                licenseText += "\n\n- - - - - - - - - - - -\nMiniDNS by Measite\n\nAvailable under the [5]Apache License 2.0[6]";
                licenseText += "\n\n- - - - - - - - - - - -\nMaterial icon pack by Google\n\nAvailable under the [7]Apache License 2.0[8]";
                licenseText += "\n\n- - - - - - - - - - - -\nGson by google\n\nAvailable under the [9]Apache License 2.0[a]";
                ClickableSpan span = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        new AlertDialog.Builder(MainActivity.this).setTitle("Apache License 2.0").setPositiveButton(R.string.close, null).setMessage(R.string.license_apache_2).show();
                    }
                }, span2 = new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        String text = getString(R.string.mit_license);
                        text = text.replace("[name]", "Pcap4J");
                        text = text.replace("[author]", "Pcap4J.org");
                        text = text.replace("[yearrange]", "2011-2017");
                        new AlertDialog.Builder(MainActivity.this).setTitle("MIT License").setPositiveButton(R.string.close, null).setMessage(text).show();
                    }
                }, span3 = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        new AlertDialog.Builder(MainActivity.this).setTitle("Apache License 2.0").setPositiveButton(R.string.close, null).setMessage(R.string.license_apache_2).show();
                    }
                }, span4 = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        new AlertDialog.Builder(MainActivity.this).setTitle("Apache License 2.0").setPositiveButton(R.string.close, null).setMessage(R.string.license_apache_2).show();
                    }
                }, span5 = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {
                        new AlertDialog.Builder(MainActivity.this).setTitle("Apache License 2.0").setPositiveButton(R.string.close, null).setMessage(R.string.license_apache_2).show();
                    }
                };
                ;
                SpannableString spannable = new SpannableString(licenseText.replaceAll("\\[.]", ""));
                spannable.setSpan(span3, licenseText.indexOf("[1]"), licenseText.indexOf("[2]") - 3, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                spannable.setSpan(span2, licenseText.indexOf("[3]") - 6, licenseText.indexOf("[4]") - 9, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                spannable.setSpan(span, licenseText.indexOf("[5]") - 12, licenseText.indexOf("[6]") - 15, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                spannable.setSpan(span4, licenseText.indexOf("[7]") - 18, licenseText.indexOf("[8]") - 21, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                spannable.setSpan(span5, licenseText.indexOf("[9]") - 24, licenseText.indexOf("[a]") - 27, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                dialog1 = new AlertDialog.Builder(MainActivity.this).setTitle(R.string.nav_title_libraries).setNegativeButton(R.string.close, null)
                        .setMessage(spannable).show();
                ((TextView) dialog1.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
                return false;
            }

            @Override
            public boolean onLongClick(DrawerItem item, NavigationDrawerActivity drawerActivity) {
                return false;
            }
        });

        itemCreator.createItemAndContinue(getString(R.string.drawer_item_purchaselifeguard), setDrawableColor(DesignUtil.getDrawable(this, R.drawable.ic_purchase)), new DrawerItem.ClickListener() {
            @Override
            public boolean onClick(DrawerItem item, NavigationDrawerActivity drawerActivity, @Nullable Bundle arguments) {

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://www.safesurfer.co.nz/product/lifeguard-mesh"));
                startActivity(intent);

                return false;
            }

            @Override
            public boolean onLongClick(DrawerItem drawerItem, NavigationDrawerActivity navigationDrawerActivity) {
                return false;
            }
        });

        itemCreator.createItemAndContinue(R.string.title_about, setDrawableColor(DesignUtil.getDrawable(this, R.drawable.ic_info)), new DrawerItem.ClickListener() {
            @Override
            public boolean onClick(DrawerItem item, NavigationDrawerActivity drawerActivity, @Nullable Bundle arguments) {
                String text = getString(R.string.about_text)
                        .replace("[[version]]", BuildConfig.VERSION_NAME)
                        .replace("[[build]]", BuildConfig.VERSION_CODE + "")
                        .replace("[[githash]]", BuildConfig.GitHash + "");
//                        .replace("[[CRASHLYTICS_ENABLED]]", BuildConfig.CRASHLYTICS_ENABLED + "")
//                        .replace("[[SSAPI_ENABLED]]", BuildConfig.SSAPI_ENABLED + "");
                ;
                new AlertDialog.Builder(MainActivity.this).setTitle(R.string.title_about).setMessage(text)
                        .setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).show();
                return false;
            }

            @Override
            public boolean onLongClick(DrawerItem item, NavigationDrawerActivity drawerActivity) {
                new AlertDialog.Builder(MainActivity.this).setMessage(R.string.easter_egg).setTitle("(╯°□°）╯︵ ┻━┻")
                        .setPositiveButton("Okay :(", null).show();
                return true;
            }
        });
        return itemCreator.getDrawerItemsAndDestroy();
    }

    DrawerItem nav_title_screencast_detection;

    AlertDialog dialogScreencastDetection;


    public static interface ScreencastDetectionDialogListener {
        public abstract void onNegative(AlertDialog dialog);

        public abstract void onNeutral(AlertDialog dialog);

        public abstract void onPositive(AlertDialog dialog);
    }

    public ScreencastDetectionDialogListener getListener() {
        return listener;
    }

    public void setListener(ScreencastDetectionDialogListener listener) {
        this.listener = listener;
    }

    ScreencastDetectionDialogListener listener;

    private void stopScreencast() {
        stopService();

        String jwtToken = Tools.getInstance().getDeviceAuthToken();
        SafeSurferNetworkApi.getInstance().getSRUseCase()
                .screencastStop(jwtToken)
                .toObservable()
                .subscribe(success -> {
                    Tools.getInstance().setCurrentScreencastId(null);
                }, fail -> {
                    fail.printStackTrace();
                });
    }

    private void startScreencast() {
        Tools.getInstance().setScreencastDetectionActive(true);
        Tools.getInstance().saveScreencastMode(Tools.SCREENCAST_MODE_WATCHING);
        Intent intent = new Intent(Tools.getInstance().getContext(), ScreenCapturerActivity.class);
        startActivity(intent);

        String jwtToken = Tools.getInstance().getDeviceAuthToken();
        if (jwtToken != null) {
            SafeSurferNetworkApi.getInstance()
                    .getSRUseCase()
                    .screencastStart(jwtToken)
                    .toObservable()
                    .subscribe(
                            success -> {
                                int responseCode = success.code();
                                switch (responseCode){
                                    case 201:
                                    case 200:
                                        jwtNeedToRefresh = false;
                                        String screencastId = success.body();
                                        Tools.getInstance().setCurrentScreencastId(screencastId);
                                        break;
                                    case 403:
                                        if(!jwtNeedToRefresh){
                                            jwtNeedToRefresh = true;
                                            refreshJWTToken();
                                        }

                                        break;
                                }
                            },
                            fail -> {
                                fail.printStackTrace();
                            });
        }
    }

    boolean jwtNeedToRefresh = false;

    private void refreshJWTToken(){
        String jwtToken = Tools.getInstance().getDeviceAuthToken();
        if(jwtToken != null){
            SafeSurferNetworkApi.getInstance()
                    .getSRUseCase()
                    .refreshJWTToken(jwtToken)
                    .toObservable()
                    .subscribe(
                            success -> {
                                switch (success.code()){
                                    case 200:
                                        String newJWTToken = Tools.removeQuotes( success.body().string() );
                                        Tools.getInstance().setDeviceAuthToken(newJWTToken);

                                        startScreencast();
                                        break;
                                }
                            },
                            fail -> {
                                fail.printStackTrace();
                            });
        }
    }

    private void createScreencastDetectionDialog() {

//        Нажатие на элемент меню показывает диалог, в названии которого стоит Screencast Detection. В Message текущий статус
//                - Turned off
//                - Turned on - 'Always'
//                - Turned on - 'Watching'

        String message = "";

        if (!Tools.getInstance().isScreencastDetectionActive()) {
            message = getString(R.string.screencast_off);
        } else {
            message = getString(R.string.message_screencast_on);
        }

        String positiveButtonLabel = "";
        String negativeButtonLabel = "Cancel";

        if (Tools.getInstance().isScreencastDetectionActive()) {
            positiveButtonLabel = "Turn off";
        } else {
            positiveButtonLabel = "Turn on";
        }

        setListener(new ScreencastDetectionDialogListener() {
            @Override
            public void onNegative(AlertDialog dialog) {
                dialog.dismiss();
            }

            @Override
            public void onNeutral(AlertDialog dialog) {
                dialog.dismiss();
            }

            @Override
            public void onPositive(AlertDialog dialog) {
                if (Tools.getInstance().isScreencastDetectionActive()) {
                    stopScreencast();
                } else {
                    startScreencast();
                }
                dialog.dismiss();
            }
        });

        dialogScreencastDetection = new AlertDialog.Builder(MainActivity.this)
                .setTitle(getResources().getString(R.string.screencast_detection_title))
                .setMessage(message)
//                .setNeutralButton(neutralButtonLabel, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        getListener().onNeutral(dialogScreencastDetection);
//                    }
//                })
                .setNegativeButton(negativeButtonLabel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getListener().onNegative(dialogScreencastDetection);
                    }
                })
                .setPositiveButton(positiveButtonLabel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getListener().onPositive(dialogScreencastDetection);

                    }
                }).create();

        dialogScreencastDetection.show();
    }

    private void openSettingsAndScrollToKey(String key) {
        if (getCurrentFragment() instanceof SettingsFragment)
            ((SettingsFragment) getCurrentFragment()).scrollToPreference(key);
        else {
            Bundle arguments = new Bundle();
            arguments.putString(SettingsFragment.ARGUMENT_SCROLL_TO_SETTING, key);
            clickItem(settingsDrawerItem, arguments);
        }
    }

    private Drawable setDrawableColor(Drawable drawable) {
        DrawableCompat.setTint(drawable.mutate(), navDrawableColor);
        return drawable;
    }

    @Override
    public StyleOptions getStyleOptions() {
        return new StyleOptions().setListItemBackgroundColor(backgroundColor)
                .setSelectedListItemTextColor(textColor)
                .setSelectedListItemColor(ThemeHandler.getColor(this, R.attr.inputElementColor, -1))
                .setListItemTextColor(textColor)
                .setListViewBackgroundColor(backgroundColor)
                .setAlphaNormal(1.0f)
                .setHeaderTextColor(textColor)
                .setAlphaSelected(1.0f);
    }

    @Override
    public boolean useItemBackStack() {
        return true;
    }

    @Override
    public int maxBackStackRecursion() {
        return 0;
    }

    public void rateApp() {
        final String appPackageName = this.getPackageName();
        LogFactory.writeMessage(this, LOG_TAG, "Opening site to rate app");
        try {
            LogFactory.writeMessage(this, LOG_TAG, "Trying to open market");
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            LogFactory.writeMessage(this, LOG_TAG, "Market was opened");
        } catch (android.content.ActivityNotFoundException e) {
            LogFactory.writeMessage(this, LOG_TAG, "Market not present. Opening with general ACTION_VIEW");
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
        Preferences.getInstance(this).put("rated", true);
    }

    private void applyDNSServersInstant() {
        if (Util.isServiceRunning(MainActivity.this)) {
            if (PreferencesAccessor.checkConnectivityOnStart(this)) {
                if (currentFragment() instanceof MainFragment) {
                    final LoadingDialog dialog = new LoadingDialog(this, R.string.checking_connectivity, R.string.dialog_connectivity_description);
                    dialog.show();
                    ((MainFragment) currentFragment()).checkDNSReachability(new MainFragment.DNSReachabilityCallback() {
                        @Override
                        public void checkFinished(@NonNull List<IPPortPair> unreachable, @NonNull List<IPPortPair> reachable) {
                            dialog.dismiss();
                            if (unreachable.size() == 0) {
                                MainActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        MainActivity.this.startService(DNSVpnService.getUpdateServersIntent(MainActivity.this, true, false));
                                    }
                                });
                            } else {
                                String _text = getString(R.string.no_connectivity_warning_text);
                                StringBuilder builder = new StringBuilder();
                                _text = _text.replace("[x]", unreachable.size() + reachable.size() + "");
                                _text = _text.replace("[y]", unreachable.size() + "");
                                boolean customPorts = PreferencesAccessor.areCustomPortsEnabled(MainActivity.this);
                                for (IPPortPair p : unreachable)
                                    builder.append("- ").append(p.formatForTextfield(customPorts)).append("\n");
                                _text = _text.replace("[servers]", builder.toString());
                                final String text = _text;
                                MainActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        new AlertDialog.Builder(MainActivity.this, ThemeHandler.getDialogTheme(MainActivity.this))
                                                .setTitle(R.string.warning).setCancelable(true).setPositiveButton(R.string.start, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                MainActivity.this.startService(DNSVpnService.getUpdateServersIntent(MainActivity.this, true, false));
                                            }
                                        }).setNegativeButton(R.string.cancel, null).setMessage(text).show();
                                    }
                                });
                            }
                        }
                    });
                } else {
                    this.startService(DNSVpnService.getUpdateServersIntent(MainActivity.this, true, false));
                }
            } else {
                this.startService(DNSVpnService.getUpdateServersIntent(MainActivity.this, true, false));
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();



        if (!startedActivity && (PreferencesAccessor.isPinProtected(this, PreferencesAccessor.PinProtectable.APP)))
            finish();
    }

    private void updatePinState(Intent intent) {
        if ((intent.getAction() != null && intent.getAction().equals(Intent.ACTION_CHOOSER)) || (intent.getComponent() != null && intent.getComponent().getPackageName().equals("com.safesurfer"))) {
            startedActivity = true;
        }
    }

    @Override
    public void startActivity(Intent intent) {
        updatePinState(intent);
        super.startActivity(intent);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        updatePinState(intent);
        super.startActivityForResult(intent, requestCode);
    }

    @Override
    public void startActivityFromFragment(@NonNull android.app.Fragment fragment, Intent intent, int requestCode) {
        updatePinState(intent);
        super.startActivityFromFragment(fragment, intent, requestCode);
    }

    @Override
    public void startActivityFromFragment(@NonNull android.app.Fragment fragment, Intent intent, int requestCode, @Nullable Bundle options) {
        updatePinState(intent);
        super.startActivityFromFragment(fragment, intent, requestCode, options);
    }

    @Override
    public void startActivityFromFragment(Fragment fragment, Intent intent, int requestCode) {
        updatePinState(intent);
        super.startActivityFromFragment(fragment, intent, requestCode);
    }

    @Override
    public void startActivityFromFragment(Fragment fragment, Intent intent, int requestCode, @Nullable Bundle options) {
        updatePinState(intent);
        super.startActivityFromFragment(fragment, intent, requestCode, options);
    }

    //Safe Surfer method to ping network every 4 minutes for updated IP address
    //20/04/2018 RB: There is another job class already in new base (ConnectivityJobAPI21.java)  that operates on network change, which is probably better that a 4 min poll.

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void beginPingUpdates() {

        ComponentName componentName = new ComponentName(this, PingService.class);

        JobInfo jobInfo = null;

        long interval = TimeUnit.MINUTES.toMillis(4);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

            jobInfo = new JobInfo.Builder(20180315, componentName)
                    .setMinimumLatency(interval)
                    .setBackoffCriteria(0, JobInfo.BACKOFF_POLICY_LINEAR)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .setPersisted(true)
                    .build();

        } else {

            jobInfo = new JobInfo.Builder(20180315, componentName)
                    .setPeriodic(interval)
                    .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                    .setPersisted(true)
                    .build();

        }

        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);

        int result = jobScheduler.schedule(jobInfo);

        if (result == JobScheduler.RESULT_SUCCESS) {

            LogFactory.writeMessage(this, LOG_TAG, "beginPingUpdates: SUCCESS");
        } else {

            LogFactory.writeMessage(this, LOG_TAG, "beginPingUpdates: FAILURE");
        }

    }


    AlertDialog dialogScreenCastIsAlways;

    private void createWarnignDialogScreenCastIsAlways() {
        dialogScreenCastIsAlways = new AlertDialog.Builder(MainActivity.this)
                .setTitle(getResources().getString(R.string.screencast_detection_title))
                .setMessage(context.getString(R.string.screencast_detection_message_is_always))
                .setPositiveButton(context.getString(R.string.screencast_detection_dialog_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogScreenCastIsAlways.dismiss();
                    }
                }).create();

        dialogScreenCastIsAlways.show();
    }


    private void stopService() {
        Tools.getInstance().setScreencastDetectionActive(false);
        stopService(new Intent(Tools.getInstance().getContext(), ScreenCapturerService.class));
        Tools.getInstance().cancelSheduleJob();
    }

    private void checkDeviceJWT(){
        String JWTToken = Tools.getInstance().getDeviceAuthToken();
        String deviceId = Tools.getInstance().getDeviceId();
        if(JWTToken == null){
//            startActivity(new Intent(this, MainActivity.class));
            return;
        }
        SafeSurferNetworkApi.getInstance().getSRUseCase().getScreencasts(
                JWTToken,
                "1",
                "100",
                deviceId
        ).subscribe(success->{
            if(success == null)return;
            switch (success.code()){
                case 200:

                    break;
                case 401:
                    Log.d(getClass().getName(), "auth error. Do register again");
                    Toast.makeText(context, R.string.auth_has_expired, Toast.LENGTH_SHORT ).show();
                    startActivity(new Intent(this, RegisterActivity.class));

                    finish();
                    break;
                default:
                    break;
            }
        }, fail->{
            fail.printStackTrace();

        });
    }

}
