package com.safesurfer.receivers;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;

import com.safesurfer.LogFactory;
import com.safesurfer.R;

/**
 * Copyright Daniel Wolf 2019<development@frostnerd.com>
 * Copyright 2019 SafeSurfer<info@safesurfer.co.nz>
 *
 * This file is part of SafeSurfer-Android.
 *
 * SafeSurfer-Android is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SafeSurfer-Android is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SafeSurfer-Android. If not, see <https:www.gnu.org/licenses/>.
 * All rights reserved.
 */
public class AdminReceiver extends DeviceAdminReceiver {
    private static final String LOG_TAG = "[AdminReceiver]";

    @Override
    public void onEnabled(Context context, Intent intent) {
        LogFactory.writeMessage(context, LOG_TAG, "Admin enabled");
    }

    @Override
    public CharSequence onDisableRequested(Context context, Intent intent) {
        LogFactory.writeMessage(context, LOG_TAG, "Disable requested");
        return context.getString(R.string.device_admin_removal_warning);
    }

    @Override
    public void onDisabled(Context context, Intent intent) {
        LogFactory.writeMessage(context, LOG_TAG, "Admin disabled");
    }

    @Override
    public void onPasswordChanged(Context context, Intent intent) {
        LogFactory.writeMessage(context, LOG_TAG, "Password changed");
    }
}